# By Alexandros Pallaris, for a master's project at Lund University. Last edited: May 25, 2021
##
## The code in this file creates lattices representing nanowires, images of the lattices, and calculates and saves conductance data for the simlations
##
##Main method contains commented code to run simulations, stuff before that is the simulation code.
##commented stuff at the bottom is more recent, and probably just improved
##
##Apologies for the mess.


import kwant
import kwant.continuum
import scipy.sparse.linalg
import scipy.linalg
import numpy as np
from matplotlib import pyplot as plt
import math
import random
from scipy.constants.constants import epsilon_0, elementary_charge, hbar, m_e
import time
from dask.dataframe.methods import loc
import os.path

verbose = True ###print out various calculation times, numbers, etc. currently does almost nothing, since I've never wanted to turn it off

pi = math.pi
degtorad = 2*pi/360 #multiply to convert degrees to radians
k = 8.617333*10**-5 ###boltzmann constant in eV/K
a = 1e-9  ##lattice constant in nm, defined as equal to one. if changing this, first make sure all calculations actually take it into account

m_eff = m_e*0.03 ##effective mass.. for In.8Ga.2As
energyToeV = 1*(hbar**2/( 2*m_eff)) / (a)**2 / elementary_charge ##t = hbar^2/(2m^* a^2) = 1, so this converts to eV
energyToJ = 1*(hbar**2/( 2*m_eff)) / (a)**2 ##same but to joules

lat = kwant.lattice.cubic(1)
#lat = kwant.lattice.general([(a, 0, 0), (0, a, 0), (0, 0, a)])


def onsite(pos, potentialList, sigma, dopePoints, overallExtra): ##given sigma in units of cm^-2.
    ans = 4 ##still 4 for 3D? just following the kwant tutorials here...
    if(sigma > 0):
        ans = ans + pot(potentialList, sigma, pos)
    if(len(dopePoints) > 0):
        if(pos in dopePoints):
            ans = ans + 1
    return ans + overallExtra  ##energy units, t = hbar^2/ (2 m a^2); units of a=1, t=1

def defaultOnsite(pos): ##as it says
    return 4

def defaultOnsiteBarrier(pos):##as it says, site in a barrier
    return 4*1.4

def pot(potentialList, sigma, pos):
    V = 0
    for pot in potentialList:
        if(pot[0] == pos):
            V = pot[1]*sigma*elementary_charge/energyToJ ##convert from [E] = J to [E] = t
            #print('PotCalculated: '+str(V))
            break
    return V       
    
def hopping(site1, site2):
    t = 1 ##energy units, t = hbar^2/ (2 m a^2); units of a=1, t=1
    return -1*t  
    
def make_wire_square(l,s): ###for comparing with analytical expression
    
    def square_shape(pos):
        x,y,z = pos
        return ( (0 <= z < l) and (0 <= y < s) and (0 <= x < s) )
    
    syst = kwant.Builder()
    syst[lat.shape(square_shape, (0, 0, 0))] = defaultOnsite
    
    return syst



def corner_shape(pos, l, t, w): ##this part checks if in the corner sections, outside make_wire so I can use this for dopant adding, too
        bufferSize = 15
        bufferSizeFactor = 1.6
        x, y, z = pos
        x = x/bufferSizeFactor
        y = y/bufferSizeFactor
        
        ## ovular
        #ansBuffer = ( ((x**2*(w/bufferSize)**2 + z**2) < bufferSize**2) and ( -t/3 <= y < t )  ) or ( ((x**2*(w/bufferSize)**2 + (z-l)**2) < bufferSize**2) and ( -t/3 <= y < t )  )
        ## ovular with flat back
        ansBuffer = ( ((x**2*(w/bufferSize)**2 + z**2) < bufferSize**2) and ( 0 <= y < t )  ) or ( ((x**2*(w/bufferSize)**2 + (z-l)**2) < bufferSize**2) and ( 0 <= y < t )  )
        ## flat back
        #ansBuffer = ( ((-bufferSize <= z < 0) or l <= z < l+bufferSize) and ( (-w/2) < x < (w/2) ) and ( 0 <= y < t )  )
        ## rectangular block
        #ansBuffer = ( ((-bufferSize <= z < 0) or l <= z < l+bufferSize) and ( (-w/2) < x < (w/2) ) and ( 0 <= y < t )  ) or ( ((-bufferSize <= z < 0) or l <= z < l+bufferSize) and ( (-w/2) < x < (w/2) ) and ( -t/2 <= y < 0 )  ) ##if in buffer
        
        return ansBuffer



    
def make_wire(l = 80, w = 20, t = 10, ac = 53*degtorad, leadToWire = 'None'):
    tana = np.tan(ac) ##wire shape calculation stuff
    w2 = t/tana

    
    def wire_shape(pos): ##defines the shape of the wire
        x, y, z = pos
        f = 1.0 ##for changing size of wire only
        x = x/f
        y = y/f
        return ( (0 <= z < l) and ( (-w/2) < x < (w/2) ) and ( 0 <= y < t ) and ( ( ( (x > (w/2-w2)) and (y < (w/2-x)*tana) ) or ( (x < (-w/2+w2)) and (y < (w/2+x)*tana) ) ) or (-w/2+w2 < x < w/2-w2 )  )  )
    
    def wire_ends_shape(pos): ##adds potential barrier to nearest 3 layers of both wire ends
        numLayers = 3
        x, y, z = pos
        return ( ((0 <= z < numLayers) or (l-numLayers <= z < l)) and ( (-w/2) < x < (w/2) ) and ( 0 <= y < t ) and ( ( ( (x > (w/2-w2)) and (y < (w/2-x)*tana) ) or ( (x < (-w/2+w2)) and (y < (w/2+x)*tana) ) ) or (-w/2+w2 < x < w/2-w2 )  )  )
    
    def wire_shape_L2W(pos): ##adds a boxy lead-to-wire connection of length bufferSize
        bufferSize = 2
        bufferSizeFactor = 2
        ansReg = wire_shape(pos) ##if in regular wire shape
        x, y, z = pos
        x = x/bufferSizeFactor
        y = y/bufferSizeFactor
        ansBuffer = ( ((-bufferSize <= z < 0) or l <= z < l+bufferSize) and ( (-w/2) < x < (w/2) ) and ( 0 <= y < t )  ) or ( ((-bufferSize <= z < 0) or l <= z < l+bufferSize) and ( (-w/2) < x < (w/2) ) and ( -t/2 <= y < 0 )  ) ##if in buffer

        return ansReg or ansBuffer
    
    def wire_shape_L2Wcorner(pos): ##adds a boxy lead-to-wire connection of length bufferSize, which here is long to allow lead in other direction, creating a 'corner'
        ansReg = wire_shape(pos) ##if in regular wire shape
        return ansReg or corner_shape(pos, l, t, w) ##corner shape defined above, outside of make_wire

    print("Creating system...")
    t1=time.time()
    
    syst = kwant.Builder()
    
    if(leadToWire == 'box'):
        syst[lat.shape(wire_shape_L2W, (0, 0, 0))] = defaultOnsite ##placeholder value, can be altered later in add_lead
    elif(leadToWire == 'corner'):
        syst[lat.shape(wire_shape_L2Wcorner, (0, 0, 0))] = defaultOnsite ##placeholder value, can be altered later in add_lead
        
        ##then print number of sites 'in the corner', to get correct doping concentration -- right after locs is created
    else:
        syst[lat.shape(wire_shape, (0, 0, 0))] = defaultOnsite ##placeholder value, can be altered later in add_lead
        
        
    #syst[lat.neighbors()] = -1
    
    
    sites = list(syst.sites())##list of all sites
    locs = [] ##list of all site locations
    ##create list of site locs, so I can give number of sites
    NsurfSites = 0
    
    Ncorner = 0 ##for printing number of sites in corner
    
    for site in sites:
        x, y, z = site.pos
        locs.append((x,y,z))
    
    for loc in locs:
        if(countNeighbours(loc, locs) < 6): ## (hopefully) selects all and only surface sites
            NsurfSites = NsurfSites + 1
        if(leadToWire == 'corner'):
            if(corner_shape(loc, l, t, w)):
                Ncorner = Ncorner + 1
                

    t2 = time.time()
    print("System Created, t="+ '{:.3e}'.format(t2-t1)+', total # of sites: '+str(len(locs))+', total # of surface sites: '+str(NsurfSites))
    if(leadToWire == 'corner'):
        print("also, # of sites 'in the corner': "+str(Ncorner))
    return syst

def add_lead(syst, l = 80, w = 20, t = 10, ac = 53*degtorad, potentialList = [], sigma = 0, bunches = [], dopePoints = [], overallExtra = 0, leadToWire = 'None'): ##this also 'finalizes' some stuff too
    print('Adding lead...')
    t1=time.time()
    
    tana = np.tan(ac) ##wire shape calculation stuff
    w2 = t/tana
    
    def lead_shape(pos): ##defines the shape of the wire
        x, y, z = pos
        f = 1.0 ##for changing size of wire only
        x = x/f
        y = y/f
        return ( ( (-w/2) < x < (w/2) ) and ( 0 <= y < t ) and ( ( ( (x > (w/2-w2)) and (y < (w/2-x)*tana) ) or ( (x < (-w/2+w2)) and (y < (w/2+x)*tana) ) ) or (-w/2+w2 < x < w/2-w2 )  )  )
  
    def lead_shape_L2W(pos): ##adds a boxy lead-to-wire connection of length bufferSize
        bufferSizeFactor = 2
        ansReg = lead_shape(pos) ##if in regular wire shape
        x, y, z = pos
        x = x/bufferSizeFactor
        y = y/bufferSizeFactor
        ansBuffer = ( ( (-w/2) < x < (w/2) ) and (( 0 <= y < t ) or ( -t/2 <= y < 0 ) )   ) ##if in buffer

        return ansReg or ansBuffer
    
    def lead_shape_L2Wcorner1(pos): ##adds a boxy lead-to-wire connection of length bufferSize, which here is long to allow lead in other direction, creating a 'corner'
        bufferSize = 15
        bufferSizeFactor = 1.6
        x, y, z = pos
        x = x/bufferSizeFactor
        
        ansBuffer = ( ((x**2*(w/bufferSize)**2 + z**2) < bufferSize**2)  ) ##if in buffer - ovular corner
        #ansBuffer = (  (-w/2) < x < (w/2)  and ( -bufferSize <= z < 0  ) ) ##if in buffer - rectangular corner

        return ansBuffer
    
    def lead_shape_L2Wcorner2(pos): ##adds a boxy lead-to-wire connection of length bufferSize-1, which here is long to allow lead in other direction, creating a 'corner'
        bufferSize = 15
        bufferSizeFactor = 1.6
        x, y, z = pos
        x = x/bufferSizeFactor
        
        ansBuffer = ( ((x**2*(w/bufferSize)**2 + (z-l)**2) < bufferSize**2)  ) ##if in buffer - ovular corner
        #ansBuffer = (  (-w/2) < x < (w/2)  and ( l <= z < l+bufferSize  ) ) ##if in buffer - rectangular corner

        return ansBuffer
    
    def wire_ends_shape(pos): ##adds potential barrier to nearest 3 layers of both wire ends
        numLayers = 3
        x, y, z = pos
        return ( ((0 <= z < numLayers) or (l-numLayers <= z < l)) and ( (-w/2) < x < (w/2) ) and ( 0 <= y < t ) and ( ( ( (x > (w/2-w2)) and (y < (w/2-x)*tana) ) or ( (x < (-w/2+w2)) and (y < (w/2+x)*tana) ) ) or (-w/2+w2 < x < w/2-w2 )  )  )
  
    sites = list(syst.sites()) ##list of all sites
    locs = [] ##list of all site locations
    ##create list of site locs, so I can check if site exists... maybe can be done more easily.
    for site in sites:
        x, y, z = site.pos
        locs.append((x,y,z))
  
    ######################### HOPPING VALUES #############################
    syst[lat.neighbors()] = hopping ##since roughness and such is added before here
    if(len(bunches) > 0):
        checkingList = []
        innerList = []
        for bunchPoint in bunches: ##check if adjacent points are in the lattice - if so, modify the hopping
            #if(bunchPoint not in locs):
            #    print('ERROR HERE')
            innerList.clear()
            x,y,z = bunchPoint
            innerList.append((x,y,z))
            innerList.append((x+1,y,z))
            innerList.append((x-1,y,z))
            innerList.append((x,y+1,z))
            innerList.append((x,y-1,z))
            innerList.append((x,y,z+1))
            innerList.append((x,y,z-1))
            checkingList.append(innerList)
            while (len(checkingList) > 0):
                curr = checkingList.pop(0)
                #print('curr:')
                #print(curr)
                #print('innerList:')
                #print(innerList)
                x,y,z = curr[0]
                for b in range(6):
                    if(curr[b+1] in locs):
                        x2,y2,z2 = curr[b+1]
                        syst[lat(x,y,z), lat(x2,y2,z2)] = -1 * 1.2 ###increase energy by a small factor here... I am currently looking through literature for something better but not hopeful
        
    
    ###change placeholder onsite values, which is 10-100 times faster than using the parameter method for this.
    ######################### ONSITE VALUES #############################
    for site in sites:
        x,y,z = site.pos
        val = onsite(site.pos, potentialList, sigma, dopePoints, overallExtra)
        #print('val:'+str(val))
        if(leadToWire == 'pot' and wire_ends_shape(site.pos)):##adds a potential-barrier connection to wire ends, with some value given here
            syst[lat(x, y, z)] = val + 0.02
        else:
            syst[lat(x, y, z)] = val
        
        
        
        
    
    if(leadToWire == 'box'): ##adds a box-shaped connection
        lead = kwant.Builder(kwant.TranslationalSymmetry((0, 0, -1)))
        lead[lat.shape(lead_shape_L2W, (0, 0, 0))] = defaultOnsite
        lead[lat.neighbors()] = hopping
        syst.attach_lead(lead)
        syst.attach_lead(lead.reversed())
        
    elif(leadToWire == 'corner'): ##lead comes in from a 90 degree angle
        lead1 = kwant.Builder(kwant.TranslationalSymmetry((0, 1, 0)))
        lead1[lat.shape(lead_shape_L2Wcorner1, (0, 0, -2))] = defaultOnsite
        lead2 = kwant.Builder(kwant.TranslationalSymmetry((0, 1, 0)))
        lead2[lat.shape(lead_shape_L2Wcorner2, (0, 0, l+2))] = defaultOnsite
        lead1[lat.neighbors()] = hopping
        lead2[lat.neighbors()] = hopping
        syst.attach_lead(lead1)
        syst.attach_lead(lead2)
        
    else:
        lead = kwant.Builder(kwant.TranslationalSymmetry((0, 0, -1)))
        lead[lat.shape(lead_shape, (0, 0, 0))] = defaultOnsite
        lead[lat.neighbors()] = hopping
        syst.attach_lead(lead)
        syst.attach_lead(lead.reversed())
        
        
    
    
    #===========================================================================
    # for dangler in syst.dangling():
    #     print('Dangling sites found: eradicating.') ##probably dont want to ever use this, since this will remove pretty much any sites added to the surface
    #     syst.eradicate_dangling()
    #     break
    #===========================================================================
    t2 = time.time()
    print("Lead added, t="+ '{:.3e}'.format(t2-t1))
    return syst

def add_lead_square(syst, s): ##this also 'finalizes' some stuff too
    print('Adding lead...')
    
    def lead_shape_square(pos): ##defines the shape of the wire
        x, y, z = pos
        return ( (0 <= y < s) and (0 <= x < s)  )
    
    syst[lat.neighbors()] = hopping
    
    lead = kwant.Builder(kwant.TranslationalSymmetry((0, 0, -1)))
    lead[lat.shape(lead_shape_square, (0, 0, 0))] = defaultOnsite
    lead[lat.neighbors()] = hopping
    
    syst.attach_lead(lead)
    syst.attach_lead(lead.reversed())

    return syst


def add_roughness(syst, l, roughness = 0.12, deepness = 1, specifiedSites = [], returnChanged = False):## first removes non-tip sites randomly from the surface of the wire, then randomly adds sites outside surface from non-removed sites. Roughness parameter increases site removal chance, deepness = 1 gives exponentially fewer removals at deeper sites
    if(roughness == 0): ##to prevent divide by zero/wasted time
        return syst, 0
    
    delSites = [] ##sites to be deleted - list containing objects of (x, y, z)
    addSites = [] ##sites to be added - list containing objects of (x, y, z)
    
    print('Adding roughness...')
    t1 =time.time()
    
    if(len(specifiedSites) == 0): ## no pre-made 'roughness list' sent in, so create own roughness
    
        sites = list(syst.sites()) ##list of all sites
        locs = [] ##list of all site locations
        ##create list of site locs, so I can check if site exists... maybe can be done more easily.
        for site in sites:
            x, y, z = site.pos
            locs.append((x,y,z))
        #print('locs l = ' + (str) (len(locs)))
        ### site removal first
        ###here try to iterate over all the wire boundary
        tempSites = []
        for site in sites: ##select all edge sites
            if(syst.degree(site) < 6): ## (hopefully) selects all and only edge sites
                #print('delSites l = ' + (str) (len(delSites)))
                x, y, z = site.pos
                pos = (x,y,z)
                tempSites = []
                tempSites = siteToDelete(l, locs, pos, roughness, deepness, delSites, n = 1)
                delSites.extend(tempSites)
                
                
                
        ##now remove duplicate items from the lists - this should not be necessary, if I understand things correctly and unless I change things later
        sitesToDel = []
        [sitesToDel.append(x) for x in delSites if x not in sitesToDel] 
        
        ### site adding second
        ###here try to iterate over all the wire boundary
        for site in sites: ##select all edge sites
            if(syst.degree(site) < 6): ## (hopefully) selects all and only edge sites
                #print('addSites l = ' + (str) (len(addSites)))
                x, y, z = site.pos
                pos = (x,y,z)
                tempSites = []
                tempSites = siteToAdd(l, locs, pos, roughness, deepness, sitesToDel, addSites, n = 0)
                addSites.extend(tempSites)
        
        sitesToAdd = []
        [sitesToAdd.append(x) for x in addSites if x not in sitesToAdd] 
        
        
        Nroughness = len(sitesToAdd) + len(sitesToDel)
        
        print('Sites to be added, then deleted ('+str(Nroughness)+' sites):')
        print(sitesToAdd)
        print(sitesToDel)
        
    
        syst = delSelectedSites(syst, sitesToDel)
        syst = addSelectedSites(syst, sitesToAdd)
        t2 = time.time()
        print('Roughness Added, t=' + '{:.3e}'.format(t2-t1))
        
    else:
        Nroughness = len(specifiedSites[0]) + len(specifiedSites[1])
        print('Sites to be added, then deleted ('+str(Nroughness)+' sites):')
        print(specifiedSites[1])
        print(specifiedSites[0])
        
        syst = delSelectedSites(syst, specifiedSites[0])
        syst = addSelectedSites(syst, specifiedSites[1])
        t2 = time.time()
        print('Roughness Added, t=' + '{:.3e}'.format(t2-t1))
    
    extraReturns = []
    if(returnChanged):
        extraReturns = [sitesToDel, sitesToAdd]
        return syst, Nroughness, extraReturns
    else:
        return syst, Nroughness

def siteToDelete(l, locs, pos, roughness, deepness, chosenSites, n): ##recursively iterates through surrounding sites with exponentially lowering probability of selection
    #print(pos)
    #print(chosenSites)
    #print(locs)
    newChosenSites = []
    x, y, z = pos
    if(0<z<(l-1)):  #to leave wire top and bottom sites to preserve lead connection and not allow wire to extend/shrink lengthwise
        if(pos not in chosenSites and pos in locs): 
            rand = random.random()
            if(rand < np.exp(-1 / roughness * (n**deepness) )): ## the random decision on whether or not a site is added/removed
                n = n+1
                
                #print('Adding to del list')
                
                newChosenSites.append(pos) ## delete the site... later
                chosenSites.append(pos)
                ##select all surrounding sites to try
                tempSites = []
                tempSites = siteToDelete(l, locs, (x-1, y, z), roughness, deepness, chosenSites, n)
                chosenSites.extend(tempSites)
                newChosenSites.extend(tempSites)
                tempSites = siteToDelete(l, locs, (x+1, y, z), roughness, deepness, chosenSites, n)
                chosenSites.extend(tempSites)
                newChosenSites.extend(tempSites)
                tempSites = siteToDelete(l, locs, (x, y-1, z), roughness, deepness, chosenSites, n)
                chosenSites.extend(tempSites)
                newChosenSites.extend(tempSites)
                tempSites = siteToDelete(l, locs, (x, y+1, z), roughness, deepness, chosenSites, n)
                chosenSites.extend(tempSites)
                newChosenSites.extend(tempSites)
                tempSites = siteToDelete(l, locs, (x, y, z-1), roughness, deepness, chosenSites, n)
                chosenSites.extend(tempSites)
                newChosenSites.extend(tempSites)
                tempSites = siteToDelete(l, locs, (x, y, z+1), roughness, deepness, chosenSites, n)
                chosenSites.extend(tempSites)
                newChosenSites.extend(tempSites)
    return newChosenSites

def siteToAdd(l, locs, pos, roughness, deepness, delSites, chosenSites, n): ##recursively iterates through surrounding sites with exponentially lowering probability of selection
    #print(pos)
    #print(chosenSites)
    #print(locs)
    newChosenSites = []
    if((pos not in chosenSites and pos not in locs and pos not in delSites) or n==0): ##first check passes because it is always a site that already exists
        x, y, z = pos
        #print(pos)
        if(0<z<l-1):  #to leave wire top and bottom sites to preserve lead connection and not allow wire to extend/shrink lengthwise
            rand = random.random()
            if(rand < np.exp(-1 / roughness * (n**deepness) )): ## the random decision on whether or not a site is added/removed
                if(n==0):
                    n = n+1
                    pass
                else:
                    n = n+1
                    #print('Adding to add list')
                    newChosenSites.append(pos)
                    chosenSites.append(pos)
                
                ##select all surrounding sites to try
                tempSites = []
                tempSites = siteToAdd(l, locs, (x-1, y, z), roughness, deepness, delSites, chosenSites, n)
                chosenSites.extend(tempSites)
                newChosenSites.extend(tempSites)
                tempSites = siteToAdd(l, locs, (x+1, y, z), roughness, deepness, delSites, chosenSites, n)
                chosenSites.extend(tempSites)
                newChosenSites.extend(tempSites)
                tempSites = siteToAdd(l, locs, (x, y-1, z), roughness, deepness, delSites, chosenSites, n)
                chosenSites.extend(tempSites)
                newChosenSites.extend(tempSites)
                tempSites = siteToAdd(l, locs, (x, y+1, z), roughness, deepness, delSites, chosenSites, n)
                chosenSites.extend(tempSites)
                newChosenSites.extend(tempSites)
                tempSites = siteToAdd(l, locs, (x, y, z-1), roughness, deepness, delSites, chosenSites, n)
                chosenSites.extend(tempSites)
                newChosenSites.extend(tempSites)
                tempSites = siteToAdd(l, locs, (x, y, z+1), roughness, deepness, delSites, chosenSites, n)
                chosenSites.extend(tempSites)
                newChosenSites.extend(tempSites)
                
    return newChosenSites

def delSelectedSites(syst, sitesToDel): ##deletes sites listed in delSites (a list of positions)
    sites = list(syst.sites())
    for site in sites:
        pos = site.pos
        for item in sitesToDel:
            if(item == pos):
                del syst[site]
    return syst

def addSelectedSites(syst, sitesToAdd): ##adds sites listed in addSites (a list of positions)
    for site in sitesToAdd:
        x, y, z = site
        syst[lat(x, y, z)] = 4
    return syst

def createSurfaceCharges(syst, l, naive = True): ##sigma is surface charge density, in units of coulombs per cm^2
    print('Creating surface charges...')
    t1=time.time()
    chg = [] ##charges as a list of positions (outside the wire) with their associated charge
    nm2cm2 = (1e2/1e9)**2 ##converts area from nm^2 to cm^2, sigma to be specified in C.cm^-2
    q =  1**2 * nm2cm2 #*sigma -- sigma multiplied into potential later, to be kept as a parameter. 1**2 = a**2 is area covered by one charge
    ##average absolute value of charge for each point
    
    sites = list(syst.sites()) 
    locs = [] ##list of all site locations
    ##create list of site locs, so I can check if site exists... maybe can be done more easily.
    for site in sites:
        x, y, z = site.pos
        locs.append((x,y,z))
    
    
    if(naive):#### simply pick every acceptable site and give it a totally random charge with an average absolute value
        ###here try to iterate over all the wire boundary
        for site in sites: ##select all edge sites
            if(syst.degree(site) < 6): ## (hopefully) selects all and only edge sites
                x, y, z = site.pos
                if(0<z<l-1):  #so that charge is not added to the top/bottom of wire
                    adjacentPoses = [(x+1,y,z),(x-1,y,z),(x,y+1,z),(x,y-1,z),(x,y,z+1),(x,y,z-1)]
                    for pos in adjacentPoses:
                        if pos not in locs: ##in case roughness added a site outside, or such
                            N = countNeighbours(pos, locs)
                            if(N == 1 or N == 2): ##presumably sites with 1 or 2 neighbours are outside the wire, good for surface charge
                                chg.append( (pos, q*( 4*random.random() - 2 )) ) ##gives selected point an evenly distributed random charge value - 'naive' method
                                #print(q*( 4*random.random() - 2 ))

    else:#### some different algorithm, presumably to have a smoother change of charge from site to site, perhaps with more intermediate sites?
        print('ERROR: DNE')
        pass
                            

    t2=time.time()
    print('Surface charges created, t= '+ '{:.3e}'.format(t2-t1))
    #print(chg)
    return chg ###list of chosen potential sites with at most 2 adjacent sites

def createDopantCharges(syst, l, probability = 0, numCharges = -1, corner = False, t = 0, w = 0, surfaceDist = -1, negativeCharge = True): ##create charges inside, 'dopant'
    print('Creating charges...')
    t1=time.time()
    chg = [] ##charges as a list of positions (outside the wire) with their associated charge
    Ndopant = 0 ###list of number picked (so far)
    if(negativeCharge):
        q = -1*elementary_charge
    else:
        q = 1*elementary_charge
    
    sites = list(syst.sites()) 
    locs = [] ##list of all site locations
    
    fullLocs = [] ##just for checking surface distance, for now
    for site in sites:
        fullLocs.append(site.pos)
        
    for site in sites:
        x, y, z = site.pos
        if(corner): ##for corner connection stuff
            if(t==0 or w==0):
                print('Error: didn\'t set wire thickness/width in createDopantCharges')
            else:
                if(corner_shape(site.pos, l, t, w)):
                    locs.append((x,y,z))
                    
        elif(surfaceDist >= 0):
            if(0 >= z or z >= l-1):
                pass #dont use sites on the ends of the wire
            elif( np.abs(distanceFromSurface(site.pos, fullLocs, l, maxDist = surfaceDist) - surfaceDist) <= 0.3 ): ##distance within .3 of a lattice constant from intended distance.. this is an arbitrary choice
                locs.append((x,y,z))         
        
        else:
            locs.append((x,y,z))
            
            
    print(str(len(locs))+' eligible sites for dopant charge creation.')
    
    if(numCharges > -1): ##randomly selects a number of sites from a list; presumably this is done actually randomly
        choices = random.sample(locs, numCharges)
        for loc in choices:
            chg.append((loc, q))
            Ndopant = Ndopant+1
        
    else:
            
        probability = probability / len(locs) ##so that average num picked is equal to probability
        for loc in locs:
            if(random.random() < probability):  ### I'm fairly sure that the list of sites is not in random order, so simply allow some random N of sites to be created
                chg.append((loc, q)) ##1 elementary_charge here
                Ndopant = Ndopant + 1
                            

    t2=time.time()
    print(str(Ndopant)+' Doping charges created, t= '+ '{:.3e}'.format(t2-t1))
    #print(chg)
    return Ndopant, chg ##returns number of added dopants, and the list of locations/charges
    
def countNeighbours(loc, locs): ###counts potential neighbours of a site. checked site does not have to exist in locs, allowing this to be used where kwant's .neighbours cannot be
    N = 0
    x, y, z = loc
    if( (x+1, y, z) in locs ):
        N = N + 1
    if( (x-1, y, z) in locs ):
        N = N + 1
    if( (x, y+1, z) in locs ):
        N = N + 1
    if( (x, y-1, z) in locs ):
        N = N + 1
    if( (x, y, z+1) in locs ):
        N = N + 1
    if( (x, y, z-1) in locs ):
        N = N + 1
    #print('# of neighbours for checked site: '+ str(N))
    return N

def dist(loc1,loc2): ##just to compute distance between 2 points, in units of lattice constant
    x1,y1,z1 = loc1
    x2,y2,z2 = loc2
    return np.sqrt((x1-x2)**2 + (y1-y2)**2 + (z1-z2)**2)

def distanceFromSurface(loc, locs, l, maxDist = -1): ##returns the distancce from the surface (for simple, gap-less lattices only) - a non-negative real. only looks in 2-D, the x-y plane
    ##if maxDist is set, that is furthest checks will go
    ##this is pretty slow to run over 10000+ sites
    
    
    
    d = -1
    x0, y0, z0 = loc
    if(countNeighbours(loc, locs) < 6):
        return 0
    elif(loc not in locs):
        print('ERROR: looking for distance from surface for a site not in the lattice') 
        return d
    elif(0 >= z0 or z0 >= l-1):
        print('ERROR: site on lead-edge') 
        return d
    else:
        checked = [] ##a list of checked points
        
        ###split check into four quadrants for simple iteration: +x+y, -x+y, +x-y, -x-y
        tLocList = [] ##list of the quadrants with points and their distances
        tLocList.append([((x0+1,y0,z0), 1), ((x0,y0+1,z0), 1)])  
        tLocList.append([((x0-1,y0,z0), 1), ((x0,y0+1,z0), 1)])   
        tLocList.append([((x0+1,y0,z0), 1), ((x0,y0-1,z0), 1)])   
        tLocList.append([((x0-1,y0,z0), 1), ((x0,y0-1,z0), 1)])
        
        finalPoints = [] ##will contain final points with their distances... I dont actually use this, though
        newList = [] ##list to replace quadrant lists after each check

        while( len(tLocList[0]) > 0 or len(tLocList[1]) > 0 or len(tLocList[2]) > 0 or len(tLocList[3]) > 0 ): ##could make this 'more efficient' and easily generalizable to extra dimensions, but I don't see much point
            
            for n in range(len(tLocList)):
                
                newList = [] ##hopefully this works... I hate pointers
                newList.clear()
                
                for point in tLocList[n]:
                        
                    ##check if point has been checked... a mediocre way to avoid redundancy. requires d > 1 so that not quadrant is left out
                    if(point[1] > 1 and ( point[0] in checked ) ):
                        pass
                    
                    elif( maxDist >= 0 and point[1] <= maxDist):

                        checked.append(point[0])
                            
                        N = countNeighbours(point[0], locs)
                        if(N < 6): ##if current point is a surface-site
                            finalPoints.append(point)
                            if(point[1] < d or d == -1): #to stop calculations from including longer distances once one has been found
                                d = point[1]
                            
                        elif( (point[1] + 1) < d or d == -1 ):
                            x, y, z = point[0]
                            testLoc1 = (x + (-1)**(n%2), y , z)
                            testLoc2 = (x, y + (-1)**(math.trunc(n/2)%2), z)
                            newList.append( ((testLoc1), dist(testLoc1, loc)) )
                            newList.append( ((testLoc2), dist(testLoc2, loc)) )
                        
                tLocList[n] = newList
                
    if(len(finalPoints) < 1):
        #print('no surface-distance points found')
        pass
    else:         
        d = finalPoints[0][1]
        for point in finalPoints:
            if(point[1] < d):
                d = point[1]
        
    #print('Point checked: d = '+str(d))
    return d ##can return -1 if no surface in maxDist range
def calculatePotentials(syst, chg, l_s = -1): ##genuinely just go through all sites of charge, for all sites in the lattice
    print('Calculating potentials...')
    t1=time.time()
    potentials = []
    if(l_s == -1): ##if it is unset, use a default value
        l_s = screeningLength()
    sites = list(syst.sites())##list of all sites
    q = 0
    for site in sites:
        x,y,z = site.pos
        potential = 0
        for charge in chg:
            q = charge[1]
            #print('charge:'+str(q))
            x2,y2,z2 = charge[0]
            r = np.sqrt( ( x2-x )**2 + ( y2-y )**2 + ( z2-z )**2 )*1e-9
            #print(r)
            if(r > .05*1e-9): ##so that a site with charge doesnt affect its own energy
                potential = potential + 1/r*np.exp(-1*r/l_s)
                
        potentials.append( ( (x,y,z), potential/(4*pi*epsilon_0)*q) )
        #print('pot:'+str(potential))
        t2=time.time()
    print('Potentials Calculated, t= '+ '{:.3e}'.format(t2-t1))
    return potentials

def screeningLength(T = 1e-3): ##just 2nm for low temp (< 10K) and 15nm for higher temps... literature makes it clear that it is unclear what length to use for this simple lattice, so chosen based on comparison of plots with data
    l = 1
    if(T > 10): ##this is literally never used so far. if using, change
        l = 15
    return l*1e-9 ##converts to nm

def createBunching(syst, bunchingness = .001): ###presumed bunching in InGaAs, described as clumps of Ga forming and increasing hopping energy 
    print('Creating bunches...')
    t1 = time.time()
    bunched = [] ##final bunched sites
    sites = list(syst.sites())##list of all sites
    locs = [] ##list of all site locations
    ##create list of site locs, so I can check if site exists... maybe can be done more easily.
    for site in sites:
        x, y, z = site.pos
        locs.append((x,y,z))
    ###go through every site, select some small number for bunching nucleation
    innerList = []
    for loc in locs:
        n = 0
        innerList.clear()
        innerList.append((loc, n))
        while(len(innerList) > 0):
            curr = innerList.pop(0)
            n = curr[1]
            if(curr[0] not in bunched and curr[0] in locs):
                if(n==0):
                    n = 1
                    bness = bunchingness*20
                else:
                    n = n+1
                    bness = bunchingness*20 / (6**n)
                
                if( random.random() < bness ): ##defaults to ~1 of sites starting bunching
                    x,y,z = curr[0]
                    bunched.append((x,y,z))
                    innerList.append(((x+1,y,z),n))
                    innerList.append(((x-1,y,z),n))
                    innerList.append(((x,y+1,z),n))
                    innerList.append(((x,y-1,z),n))
                    innerList.append(((x,y,z+1),n))
                    innerList.append(((x,y,z-1),n))
                
    t2 = time.time()
    print(str(len(bunched))+' Bunched: t = '+str(t2-t1))
    return bunched

def addDopants(syst, n = 1e16): ###runs through entire system, randomly selecting points to 'replace with a dopant atom'
    dopePoints = []
    chance = ( (1e2/1e9)**3 )*n ##assume n in per cm^3
    sites = list(syst.sites())##list of all sites
    for site in sites:
        if(random.random() < chance):
            dopePoints.append(site.pos)
    print(str(len(dopePoints))+' Dopants added: t is no doubt small for this action')
    return dopePoints

#===============================================================================
# def createInitWireList(pos):
#         wireList = []
#         x, y, z = pos
#         if( ( (-w/2) < x < (w/2) ) and ( 0 <= y < t ) and ( ( ( (x > (w/2-w2)) and (y < (w/2-x)*tana) ) or ( (x < (-w/2+w2)) and (y < (w/2+x)*tana) ) ) or (-w/2+w2 < x < w/2-w2 )  )  )
#             wireList.append(pos)
#             wireList = createInitWireList((x-1, y, z))
#             wireList = createInitWireList((x+1, y, z))
#             wireList = createInitWireList((x, y+1, z))
#             wireList = createInitWireList((x, y-1, z))
#             wireList = createInitWireList((x, y, z+1))
#             wireList = createInitWireList((x, y, z-a))
#         return wireList
#===============================================================================

def plotConductance(syst, energies, filename = '', fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/'):
    # Compute conductance
    data = []
    for energy in energies:
        smatrix = kwant.smatrix(syst, energy)
        data.append(smatrix.transmission(1, 0))

    plt.figure()
    plt.plot(energies, data)
    plt.xlabel("energy [t]")
    plt.ylabel("conductance [e^2/h]")
    if(len(filename) == 0):
        plt.show()
    else:
        plt.savefig(fileLoc+'condPlot'+filename+'.png')
    plt.clf()
    
    
    


if __name__ == '__main__':###MAIN HERE
    print('Start')
timeStart=time.time()
l = 60
w = 20
t = 10
plotSiteSize = 0.45
offsetEnergy = 1.9

print('t to eV: '+str(energyToeV))
print('t to J: '+str(energyToJ))

fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/doping/RUN1'



#===============================================================================
# syst = make_wire(l = l,w = w,t = t)
# 
# #DO TWO RUNS AND SAVES THEM
# testSys = add_lead(syst, l, w, t)
# kwant.plot(testSys, site_size=plotSiteSize)
# plt.clf()
# plotConductance(testSys.finalized(), energies=[-offsetEnergy + 0.01 * i for i in range(40)])
# plt.clf()
# systN, Nroughness = add_roughness(syst, l, roughness = 0.01)
# 
# syst = make_wire(l = l,w = w,t = t)
# testSys = add_lead(syst, l, w, t)
# kwant.plot(testSys, site_size=plotSiteSize)
# plt.clf()
# plotConductance(testSys.finalized(), energies=[-offsetEnergy + 0.01 * i for i in range(40)])
#===============================================================================


####RUNS WITH INCREASING VALUES, SAVED TOGETHER
datas = []
Nroughnesses = []
Nroughness = 0
energies=[-offsetEnergy + 0.003 * i for i in range(240)]
energiesIneV = [] #for plotting in different units
for i in range(len(energies)): ##there must be a better way to do this
    energiesIneV.append( energies[i]*energyToeV )
sigmas=[0, 1e9*elementary_charge, 1e10*elementary_charge, 5e10*elementary_charge, 1e11*elementary_charge, 5e11*elementary_charge, 1e12*elementary_charge, 5e12*elementary_charge, 1e13*elementary_charge, 1e14*elementary_charge, 1e15*elementary_charge]
sigma = 0

#===============================================================================
# ##runs with increasing roughness
# for b in range(2): ###first save individual plots, then all-together plot 
#     print('b = '+str(b))
#     for N in range(7): ##run number
#           
#         if(b == 0):
#             print('N = '+str(N))
#             syst = make_wire(l = l,w = w,t = t)
#             if(N > 0):
#                 testSys, Nroughness = add_roughness(syst, l, roughness = .3 + .2*N)
#             else:
#                 testSys = syst
#             Nroughnesses.append(Nroughness)
#             testSys = add_lead(testSys, l, w, t)
#                 
#             kwant.plot(testSys, site_size=plotSiteSize, file=fileLoc+'wireIMG_run2-'+str(N)+'.png')
#             data = []
#             data.clear()
#             testSys = testSys.finalized()
#             for energy in energies:
#                 smatrix = kwant.smatrix(testSys, energy)
#                 data.append(smatrix.transmission(1, 0))
#             datas.append(data)
#             
#           
#             filename = 'RUN2-'+str(N)+'_r='+str(Nroughnesses[N])+'_l='+(str(l))+'_w='+(str(w))+'_t='+(str(t))
#             plt.figure()
#             plt.grid()
#             plt.plot(energiesIneV, datas[N])
#             plt.xlabel("energy [eV]")
#             plt.ylabel("conductance [e^2/h]")
#             plt.savefig(fileLoc+'condPlot'+filename+'.png')
#             plt.clf()
#                 
#         elif(b == 1):
#             filename = 'RUN2-total_l='+(str(l))+'_w='+(str(w))+'_t='+(str(t))
#             plt.figure()
#             plt.grid()
#             for y in range(len(Nroughnesses)):
#                 plt.plot(energiesIneV, datas[y], label='R = '+str(.15 + .05*y)+', N = '+str(Nroughnesses[y]), c=str(.05+ .9/len(Nroughnesses)*y))
#             plt.xlabel("energy [eV]")
#             plt.ylabel(r"conductance [$e^2$/h]")
#             #plt.legend()
#             plt.savefig(fileLoc+'condPlot'+filename+'.png')
#             plt.clf()
#                 
#         plt.clf()
#===============================================================================


#===============================================================================
# syst = make_wire(l = l,w = w,t = t) ###runs with increasing surface charge
#  
# surfaceCharges = createSurfaceCharges(syst, l)
# potentials = calculatePotentials(syst, surfaceCharges)
# testSys= add_lead(syst, l, w, t)
#  
# kwant.plot(testSys, site_size=plotSiteSize, file=fileLoc+'wireIMG_run.png')
# testSys = testSys.finalized()
#  
#  
# for N in range (len(sigmas)):
#     syst = make_wire(l = l,w = w,t = t)
#     sigma = sigmas[N]
#     print('sigma = '+'{:.3e}'.format(sigma))
#     testSys= add_lead(syst, l, w, t, potentialList = potentials, sigma = sigma)
#     testSys=testSys.finalized()
#     data = []
#     data.clear()
#     for energy in energies:
#         #print('energy = '+str(energy))
#         smatrix = kwant.smatrix(testSys, energy)
#         data.append(smatrix.transmission(1, 0))
#     datas.append(data)
#      
#     filename = 'RUN'+ '{:.3e}'.format(sigma)+'_l='+(str(l))+'_w='+(str(w))+'_t='+(str(t))
#     plt.figure()
#     plt.grid()
#     plt.plot(energiesIneV, datas[N])
#     plt.xlabel("energy [eV]")
#     plt.ylabel("conductance [e^2/h]")
#     plt.savefig(fileLoc+'condPlot'+filename+'.png')
#  
#  
# filename = 'RUNtotal_l='+(str(l))+'_w='+(str(w))+'_t='+(str(t))
# plt.figure()
# plt.grid()
#  
# for y in range(len(sigmas)):
#     plt.plot(energiesIneV, datas[y], label=r'$\sigma$ = '+ '{:.3e}'.format(sigmas[y]) )
# plt.xlabel("energy [eV]")
# plt.ylabel(r"conductance [$e^2$/h]")
# plt.title(r'Conductance by energy for different surface charges (in units of $cm^{-2}$)')
# plt.legend()
# plt.savefig(fileLoc+'condPlot'+filename+'.png')
#===============================================================================


#===============================================================================
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/doping/RUN2'
# syst = make_wire(l = l,w = w,t = t) ###runs with increasing doping
#   
# 
# n = [1e17,5e17,1e18,5e18,1e19,5e19,1e20]
# 
# testSys= add_lead(syst, l, w, t)
#   
# kwant.plot(testSys, site_size=plotSiteSize, file=fileLoc+'wireIMG_run.png')
# testSys = testSys.finalized()
#   
#   
# for N in range (len(n)):
#     syst = make_wire(l = l,w = w,t = t)
#     nhere = n[N]
#     doping = addDopants(syst, n = nhere)
#     print('n = '+'{:.3e}'.format(nhere))
#     testSys= add_lead(syst, l, w, t, dopePoints=doping)
#     testSys=testSys.finalized()
#     data = []
#     data.clear()
#     for energy in energies:
#         #print('energy = '+str(energy))
#         smatrix = kwant.smatrix(testSys, energy)
#         data.append(smatrix.transmission(1, 0))
#     datas.append(data)
#       
#     filename = 'RUN'+ '{:.3e}'.format(nhere)+'_l='+(str(l))+'_w='+(str(w))+'_t='+(str(t))
#     plt.figure()
#     plt.grid()
#     plt.plot(energiesIneV, datas[N])
#     plt.xlabel("energy [eV]")
#     plt.ylabel("conductance [e^2/h]")
#     plt.savefig(fileLoc+'condPlot'+filename+'.png')
#   
#   
# filename = 'RUNtotal_l='+(str(l))+'_w='+(str(w))+'_t='+(str(t))
# plt.figure()
# plt.grid()
#   
# for y in range(len(n)):
#     plt.plot(energiesIneV, datas[y], label=r'n = '+ '{:.3e}'.format(n[y]) )
# plt.xlabel("energy [eV]")
# plt.ylabel(r"conductance [$e^2$/h]")
# plt.title(r'Conductance by $n_{dopant}$ for different ns (in units of $cm^{-3}$)')
# plt.legend()
# plt.savefig(fileLoc+'condPlot'+filename+'.png')
#===============================================================================





#===============================================================================
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/bunching/RUN1'
# syst = make_wire(l = l,w = w,t = t) ###runs with increasing bunchings
#   
# 
# bnesses = [.0005,.001,.002,.003,.004,.005,.006,.01]
# 
# testSys= add_lead(syst, l, w, t)
#   
# kwant.plot(testSys, site_size=plotSiteSize, file=fileLoc+'wireIMG_run.png')
# testSys = testSys.finalized()
#   
#   
# for N in range (len(bnesses)):
#     syst = make_wire(l = l,w = w,t = t)
#     bunchingness = bnesses[N]
#     bunchPoints = createBunching(syst, bunchingness = bunchingness)
#     print('bunchingness = '+'{:.3e}'.format(bunchingness))
#     testSys= add_lead(syst, l, w, t, bunches = bunchPoints)
#     testSys=testSys.finalized()
#     data = []
#     data.clear()
#     for energy in energies:
#         #print('energy = '+str(energy))
#         smatrix = kwant.smatrix(testSys, energy)
#         data.append(smatrix.transmission(1, 0))
#     datas.append(data)
#     
#     filename = 'RUN'+ '{:.3e}'.format(bunchingness)+'_l='+(str(l))+'_w='+(str(w))+'_t='+(str(t))
#     plt.figure()
#     plt.grid()
#     plt.plot(energiesIneV, datas[N])
#     plt.xlabel("energy [eV]")
#     plt.ylabel("conductance [e^2/h]")
#     plt.savefig(fileLoc+'condPlot'+filename+'.png')
#   
#   
# filename = 'RUNtotal_l='+(str(l))+'_w='+(str(w))+'_t='+(str(t))
# plt.figure()
# plt.grid()
#   
# for y in range(len(bnesses)):
#     plt.plot(energiesIneV, datas[y], label=r'n = '+ '{:.3e}'.format(bnesses[y]) )
# plt.xlabel("energy [eV]")
# plt.ylabel(r"conductance [$e^2$/h]")
# plt.title(r'Conductance by \'bunchingess\' for different values')
# plt.legend()
# plt.savefig(fileLoc+'condPlot'+filename+'.png')
#===============================================================================


#===============================================================================
# syst = make_wire_square(l=40, s=10) ###MAKE A SQUARE WELL AND SHOW ENERGIES
# syst = add_lead_square(syst, s=10)
# kwant.plot(syst, site_size=plotSiteSize)
#     
# offsetEnergy = 2.1
# energies=[-offsetEnergy + 0.002 * i for i in range(450)]
#   
# addFac = (.27854-0.1114)/(.256306-0.1114) ###changes square nanowire energies to match infinite well energies
# fac = hbar**2 /2 /m_eff / elementary_charge * 10**18 * addFac
# energiesIneV = [] #for plotting in different units
# for i in range(len(energies)): ##there must be a better way to do this
#     energiesIneV.append( energies[i]*fac )
#         
# syst = syst.finalized()
#     
# data = []
# for energy in energies:
#     smatrix = kwant.smatrix(syst, energy)
#     data.append(smatrix.transmission(1, 0))
#  
# plt.figure()
# plt.grid()
# plt.plot(energies, data)
# plt.xlabel("energy [eV]", fontsize = '15')
# plt.ylabel(r"conductance [$e^2$/h]", fontsize = '15')
# plt.title(r'Conductance by Energy for Square Nanowire', fontsize = '17')
# plt.legend()
# plt.show()
#===============================================================================



####quick plot to get some images of lattice roughness
l = 70
w = 24
t = 12
plotSiteSize = 0.45
 
 
 
#===============================================================================
# roughnesses=[.08, .12, .15, .18, .22, .25, .28, .31, .35, .4, .45, .5, .6]
#  
# for N in range(len(roughnesses)): ##run number
#     roughness = roughnesses[N]
#     syst = make_wire(l = l,w = w,t = t)
#     syst, Nroughness = add_roughness(syst, l, roughness)
#     syst = add_lead(syst, l, w, t)
#        
#     kwant.plot(syst, site_size=plotSiteSize)
#===============================================================================



#===============================================================================
# ###################################################################################################################
# ################### CALCULATE FOR VARIOUS VARIATIONS JUST NEAR THE FIRST CONDUCTANCE EDGE, TO GET EDGE BEHAVIOUR
# ################### UNMODIFIED RUN IS THE FIRST BUNCHING RUN
# ###################################################################################################################
#   
#  
# l = 70
# w = 24
# t = 12
# plotSiteSize = 0.45
# offsetEnergy = 1.95
#   
# datas = []
# Nroughnesses = []
# Nroughness = 0
# energies=[-offsetEnergy + 0.0003 * i for i in range(1460)]
# energiesIneV = [] #for plotting in different units
# for i in range(len(energies)): ##there must be a better way to do this
#     energiesIneV.append( energies[i]*energyToeV )
#  
#  
#  
# 
#  
#  
# #===============================================================================
# # datas.clear()
# # n = [1e16, 5e16, 1e17, 5e17, 1e18, 3e18, 6e18, 1e19, 5e19, 1e20, 5e20, 1e21]###runs with increasing doping
# #  
# #  
# # for N in range (len(n)):
# #     syst = make_wire(l = l,w = w,t = t)
# #     nhere = n[N]
# #     doping = addDopants(syst, n = nhere)
# #     print('n = '+'{:.3e}'.format(nhere))
# #     testSys= add_lead(syst, l, w, t, dopePoints=doping)
# #     testSys=testSys.finalized()
# #     data = []
# #     data.clear()
# #     for energy in energies:
# #         #print('energy = '+str(energy))
# #         smatrix = kwant.smatrix(testSys, energy)
# #         data.append(smatrix.transmission(1, 0))
# #     datas.append(data)
# #         
# # fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/definitive_runs_hopefully/dopingruns.npy'
# # np.save(fileLoc, datas)
# #===============================================================================
#  
#  
#  
#  
# #===============================================================================
# # fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/definitive_runs_hopefully/'
# # ##runs with increasing roughness
# # datas.clear()
# # roughnesses=[.08, .12, .15, .18, .22, .25, .28, .31, .35, .4, .45, .5]
# # Nroughnesses = []
# # for N in range(len(roughnesses)): ##run number
# #     roughness = roughnesses[N]
# #     syst = make_wire(l = l,w = w,t = t)
# #      
# #     testSys, Nroughness = add_roughness(syst, l, roughness)
# #     Nroughnesses.append(Nroughness)
# #     testSys = add_lead(testSys, l, w, t)
# #      
# #     kwant.plot(testSys, site_size=plotSiteSize, file=fileLoc+'wireIMG_roughnessRun-'+str(N)+'.png')
# #     data = []
# #     data.clear()
# #     testSys = testSys.finalized()
# #     for energy in energies:
# #         smatrix = kwant.smatrix(testSys, energy)
# #         data.append(smatrix.transmission(1, 0))
# #     datas.append(data)
# #       
# #  
# # fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/definitive_runs_hopefully/roughnessruns.npy'
# # np.save(fileLoc, datas)
# # fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/definitive_runs_hopefully/roughnessrunsNchangedsites.npy'
# # np.save(fileLoc, Nroughnesses)
# #===============================================================================
#  
#  
#  
#  
#  
# #===============================================================================
# # datas.clear()
# # syst = make_wire(l = l,w = w,t = t) ###runs with increasing surface charge
# # sigmas=[1e9*elementary_charge, 4e9*elementary_charge, 8e9*elementary_charge,  1.2e10*elementary_charge, 3e10*elementary_charge, 6e10*elementary_charge, 1e11*elementary_charge, 3e11*elementary_charge, 6e11*elementary_charge, 1e12*elementary_charge, 5e12*elementary_charge, 1e13*elementary_charge]
# # sigma = 0
# #   
# # surfaceCharges = createSurfaceCharges(syst, l)
# # potentials = calculatePotentials(syst, surfaceCharges)
# #   
# #     
# # for N in range (len(sigmas)):
# #     syst = make_wire(l = l,w = w,t = t)
# #     sigma = sigmas[N]
# #     print('sigma = '+'{:.3e}'.format(sigma))
# #     testSys= add_lead(syst, l, w, t, potentialList = potentials, sigma = sigma)
# #     testSys=testSys.finalized()
# #     data = []
# #     data.clear()
# #     for energy in energies:
# #         #print('energy = '+str(energy))
# #         smatrix = kwant.smatrix(testSys, energy)
# #         data.append(smatrix.transmission(1, 0))
# #     datas.append(data)
# #         
# #   
# # fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/definitive_runs_hopefully/surfacechargeruns.npy'
# # np.save(fileLoc, datas)
# #===============================================================================
#  
#  
#  
#  
# syst = make_wire(l = l,w = w,t = t) ###runs with increasing bunching
#   
# bnesses = [0, .00003, .00006, .0001, .0004, .0008, .0014, .002, .003, .0035, .004, .005, .006, .01]
# testSys= add_lead(syst, l, w, t)
# testSys = testSys.finalized()
#      
# for N in range (len(bnesses)):
#     syst = make_wire(l = l,w = w,t = t)
#     bunchingness = bnesses[N]
#     bunchPoints = createBunching(syst, bunchingness = bunchingness)
#     print('bunchingness = '+'{:.3e}'.format(bunchingness))
#     testSys= add_lead(syst, l, w, t, bunches = bunchPoints)
#     testSys=testSys.finalized()
#     data = []
#     data.clear()
#     for energy in energies:
#         #print('energy = '+str(energy))
#         smatrix = kwant.smatrix(testSys, energy)
#         data.append(smatrix.transmission(1, 0))
#     datas.append(data)
#   
#   
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/definitive_runs_hopefully/bunchingruns.npy'
# np.save(fileLoc, datas)
#  
# ########################
# #EDGE RUNS END
# #######################
#===============================================================================



#===============================================================================
# ##############################
# ####try some runs with an extremely wide 'wire'/quantum well... see how long it takes
# #### AKA WIDE-WIRE BEGIN
# ##############################
# calct1 = time.time()
# 
# 
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/widewireruns/'
# l = 25
# w = 250
# t = 5
# plotSiteSize = 0.45
# offsetEnergy = 1.83
# 
# energies=[-offsetEnergy + 0.002 * i for i in range(500)]
# 
# 
# #===============================================================================
# # np.save(fileLoc+'widewirelargerangeEs.npy', energies)
# # 
# # sigmas=[4e9*elementary_charge, 8e9*elementary_charge,  1.2e10*elementary_charge, 3e10*elementary_charge, 6e10*elementary_charge, 0]
# # sigma = 0
# # syst = make_wire(l = l,w = w,t = t)
# # surfaceCharges = createSurfaceCharges(syst, l)
# # potentials = calculatePotentials(syst, surfaceCharges)
# # 
# # 
# # 
# # for N in range (len(sigmas)):
# #     syst = make_wire(l = l,w = w,t = t)
# #     sigma = sigmas[N]
# #     print('sigma = '+'{:.3e}'.format(sigma))
# #     testSys= add_lead(syst, l, w, t, potentialList = potentials, sigma = sigma)
# #     kwant.plot(testSys, site_size=plotSiteSize, file=fileLoc+'wireIMG.png')
# #     testSys=testSys.finalized()
# #     data = []
# #     data.clear()
# #     for energy in energies:
# #         #print('energy = '+str(energy))
# #         smatrix = kwant.smatrix(testSys, energy)
# #         data.append(smatrix.transmission(1, 0))
# # 
# #     np.save(fileLoc+'widewirelargerange'+str(N)+'.npy', data)
# # 
# # 
# # 
# # energies=[-offsetEnergy + 0.0005 * i for i in range(500)]
# # 
# # 
# # np.save(fileLoc+'widewiresmallrangeEs.npy', energies)
# # 
# # 
# # 
# # for N in range (len(sigmas)):
# #     syst = make_wire(l = l,w = w,t = t)
# #     sigma = sigmas[N]
# #     print('sigma = '+'{:.3e}'.format(sigma))
# #     testSys= add_lead(syst, l, w, t, potentialList = potentials, sigma = sigma)
# #     kwant.plot(testSys, site_size=plotSiteSize, file=fileLoc+'wireIMG.png')
# #     testSys=testSys.finalized()
# #     data = []
# #     data.clear()
# #     for energy in energies:
# #         #print('energy = '+str(energy))
# #         smatrix = kwant.smatrix(testSys, energy)
# #         data.append(smatrix.transmission(1, 0))
# # 
# #     np.save(fileLoc+'widewiresmallrange'+str(N)+'.npy', data)
# #===============================================================================
# 
# 
# #===============================================================================
# # l = 2 ##since length is semi-infinite anyway, doesnt matter with pristine wire
# # w = 250
# # t = 5
# # plotSiteSize = 0.45
# # offsetEnergy = 1.83
# # energies=[-offsetEnergy + 0.0002 * i for i in range(8000)]
# # 
# # syst = make_wire(l = l,w = w,t = t)
# # testSys = add_lead(syst, l, w, t)
# # testSys=testSys.finalized()
# # data = []
# # data.clear()
# # for energy in energies:
# #     print('energy = '+str(energy))
# #     smatrix = kwant.smatrix(testSys, energy)
# #     data.append(smatrix.transmission(1, 0))
# # 
# # np.save(fileLoc+'widewirepristinerun.npy', data)
# # np.save(fileLoc+'widewirepristinerunEs.npy', energies)
# #===============================================================================
# 
# calct2 = time.time()
# print('Wide-Wire execution time: t= '+ '{:.3e}'.format(calct2-calct1)+' s')
# ##############################
# ####WIDE-WIRE END
# ##############################
#===============================================================================

l = 70
w = 24
t = 12
plotSiteSize = 0.45
offsetEnergy = 1.95
datas = []
Nroughnesses = []
Nroughness = 0
energies=[-offsetEnergy + 0.003 * i for i in range(240)]
energiesIneV = np.array(energies) * energyToeV
   
datas.clear()



#===============================================================================
# syst = make_wire(l = l,w = w,t = t) ###runs with increasing surface charge, changing screening length
# sigmas=np.array([0, 1e11, 1e12, 1e13, 1e14, 1e15, 1e16, 1e17])*elementary_charge
# screeningLengths = np.array([.05, .1, .5, 1, 2, 3.5, 6, 10, 15, 20, 30])*1e-9  ## in nm
#         
# surfaceCharges = createSurfaceCharges(syst, l)
#              
# for J in range(len(screeningLengths)):
#     print('############l_s = '+'{:.3e}'.format(screeningLengths[J]))
#     for N in range (len(sigmas)):
#         syst = make_wire(l = l,w = w,t = t)
#         potentials = calculatePotentials(syst, surfaceCharges, l_s = screeningLengths[J])
#         sigma = sigmas[N]
#         print('sigma = '+'{:.3e}'.format(sigma))
#         testSys= add_lead(syst, l, w, t, potentialList = potentials, sigma = sigma)
#         testSys=testSys.finalized()
#         data = []
#         data.clear()
#         for energy in energies:
#             #print('energy = '+str(energy))
#             smatrix = kwant.smatrix(testSys, energy)
#             data.append(smatrix.transmission(1, 0))
#         print(np.shape(data))
#         datas.append(data)
#         print(np.shape(datas))
#     ##try this 2
#     fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/newstuff/SCcsrnlngthscondata'+str(J)+'.npy' ##try this, since you can't simply save triply nested lists
#     np.save(fileLoc, datas)
#     datas.clear()
#===============================================================================
                 



#===============================================================================
# ###runs with increasing inside charge/doping
# nums=np.array([0, 1, 2, 3, 4, 5, 7, 9, 11, 15, 21, 30, 40, 55, 80, 100])
# datas = []
# datas.clear()
#        
# Ndopants = []
# sigma = 1 ##meaningless, just =1 for calculation
#        
# for N in range (len(nums)):
#     syst = make_wire(l = l,w = w,t = t)
#     Ndopant, dopantCharges = createDopantCharges(syst, l, numCharges = nums[N])
#     Ndopants.append(Ndopant)
#     potentials = calculatePotentials(syst, dopantCharges)
#          
#     print('num = '+'{:.3e}'.format(nums[N]))
#     testSys= add_lead(syst, l, w, t, potentialList = potentials, sigma = sigma)
#     testSys=testSys.finalized()
#     data = []
#     data.clear()
#     for energy in energies:
#         #print('energy = '+str(energy))
#         smatrix = kwant.smatrix(testSys, energy)
#         data.append(smatrix.transmission(1, 0))
#     datas.append(data)
#                     
#            
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/newstuff1nm/inDopesPos.npy'
# saves = [datas, energiesIneV, Ndopants]
# np.save(fileLoc, saves)
#===============================================================================


#===============================================================================
# syst = make_wire(l = l,w = w,t = t, leadToWire='corner')
# syst = add_lead(syst, l, w, t, leadToWire = 'corner')
# kwant.plot(syst, site_size=plotSiteSize)
# 
# syst=syst.finalized()
# data = []
# data.clear()
# for energy in energies:
#     #print('energy = '+str(energy))
#     smatrix = kwant.smatrix(syst, energy)
#     data.append(smatrix.transmission(1, 0))
# 
# 
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/newstuff/connectcorner.npy'
# saves = [data, energiesIneV]
# np.save(fileLoc, saves)
#===============================================================================


#===============================================================================
# sizes = [1,.8,.6, .4, .2]
# datas = []
# datas.clear()
#  
# energies=[-offsetEnergy + 0.0015 * i for i in range(190)]
# energiesIneV = np.array(energies) * energyToeV
# 
# for N in range(len(sizes)):
#       
#     l = 70 *sizes[N]
#     w = 24 *sizes[N]
#     t = 12 *sizes[N]
#   
#     syst = make_wire(l = l,w = w,t = t, leadToWire='box')
#     syst = add_lead(syst, l, w, t, leadToWire = 'box')
#     kwant.plot(syst, site_size=plotSiteSize, file = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/newstuff/largerwireconnectboxsize'+str(sizes[N])+'.png')
#        
#     syst=syst.finalized()
#     data = []
#     data.clear()
#     for energy in energies:
#         #print('energy = '+str(energy))
#         smatrix = kwant.smatrix(syst, energy)
#         data.append(smatrix.transmission(1, 0))
#     datas.append(data)
#        
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/newstuff/largerwireconnectboxsize0.2.npy'
# saves = [datas, energiesIneV, sizes]
# np.save(fileLoc, saves)
#  
# datas = []
# datas.clear()
# for N in range(len(sizes)):
#       
#     l = 70 *sizes[N]
#     w = 24 *sizes[N]
#     t = 12 *sizes[N]
#       
#     syst = make_wire(l = l,w = w,t = t, leadToWire='corner')
#     syst = add_lead(syst, l, w, t, leadToWire = 'corner')
#     kwant.plot(syst, site_size=plotSiteSize, file = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/newstuff/largerwireconnectcornersize'+str(sizes[N])+'.png')
#         
#     syst=syst.finalized()
#     data = []
#     data.clear()
#     for energy in energies:
#         #print('energy = '+str(energy))
#         smatrix = kwant.smatrix(syst, energy)
#         data.append(smatrix.transmission(1, 0))
#     datas.append(data)
#     
#     
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/newstuff/largerwireconnectcornersize0.2.npy'
# saves = [datas, energiesIneV, sizes]
# np.save(fileLoc, saves)
#===============================================================================
 
 
#===============================================================================
# energies=[-offsetEnergy + 0.003 * i for i in range(400)]
# energiesIneV = np.array(energies) * energyToeV
#   
# overallValues = np.array([.005, .01, .025, .05, .075, .1, .15, .2, .25, .3,])*-1
# datas = []
# for N in range(len(overallValues)):
#     syst = make_wire(l = l,w = w,t = t)
#     syst = add_lead(syst, l, w, t, overallExtra=.1*N)
#          
#     syst=syst.finalized()
#     data = []
#     data.clear()
#     for energy in energies:
#         #print('energy = '+str(energy))
#         smatrix = kwant.smatrix(syst, energy)
#         data.append(smatrix.transmission(1, 0))
#     datas.append(data)
#      
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/newstuff/overalldopingNeg.npy'
# saves = [datas, energiesIneV, overallValues]
# np.save(fileLoc, saves)
#   
# overallValues = np.array([.005, .01, .025, .05, .075, .1, .15, .2, .25, .3,])
# datas = []
# for N in range(len(overallValues)):
#     syst = make_wire(l = l,w = w,t = t)
#     syst = add_lead(syst, l, w, t, overallExtra=.1*N)
#          
#     syst=syst.finalized()
#     data = []
#     data.clear()
#     for energy in energies:
#         #print('energy = '+str(energy))
#         smatrix = kwant.smatrix(syst, energy)
#         data.append(smatrix.transmission(1, 0))
#     datas.append(data)
#      
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/newstuff/overalldoping.npy'
# saves = [datas, energiesIneV, overallValues]
# np.save(fileLoc, saves)
#===============================================================================


#===============================================================================
# datas = []
# datas.clear()
# offsetEnergy = 1.95
# energies=[-offsetEnergy + 0.003 * i for i in range(240)]
# energiesIneV = np.array(energies) * energyToeV
# nums=np.array([0, 1, 3, 5, 8, 11, 15, 21, 35, 60, 90, 200, 400, 550, 800])
# sigma = 1 ##meaningless, just =1 for calculation
# Ndopants = []
# Ndopants.clear()
# l = 70
# w = 24
# t = 12
#        
# syst = make_wire(l = l,w = w,t = t, leadToWire='corner')
# syst = add_lead(syst, l, w, t, leadToWire = 'corner')
# kwant.plot(syst, site_size=plotSiteSize)
#  
#   
# for N in range (len(nums)):
#     syst = make_wire(l = l,w = w,t = t, leadToWire='corner')
#     Ndopant, dopantCharges = createDopantCharges(syst, l, numCharges = nums[N], corner = True, w=w, t=t, negativeCharge = True)
#     Ndopants.append(Ndopant)
#     potentials = calculatePotentials(syst, dopantCharges)
#            
#     print('num dopants = '+'{:.3e}'.format(Ndopants[N]))
#     testSys= add_lead(syst, l, w, t, potentialList = potentials, sigma = sigma, leadToWire = 'corner')
#     testSys=testSys.finalized()
#     data = []
#     data.clear()
#     for energy in energies:
#         #print('energy = '+str(energy))
#         smatrix = kwant.smatrix(testSys, energy)
#         data.append(smatrix.transmission(1, 0))
#     datas.append(data)
#      
#           
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/newstuff1nm/connectcornerdopedneg.npy'
# saves = [datas, energiesIneV, Ndopants]
# np.save(fileLoc, saves)
#===============================================================================


#===============================================================================
# ##some more wide wire runs
#    
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/newstuff1nm/wideWireLarge'
# l = 25
# w = 250
# t = 5
# plotSiteSize = 0.45
# offsetEnergy = 1.83
#    
#     
# sigmas=np.array([0, 4e11, 1e12, 5e12, 1e13, 5e13, 1e14])*elementary_charge
# sigma = 0
# syst = make_wire(l = l,w = w,t = t)
# surfaceCharges = createSurfaceCharges(syst, l)
# potentials = calculatePotentials(syst, surfaceCharges)
# kwant.plot(syst, site_size=plotSiteSize)
#     
# offsetEnergy = 1.73
# energies = [-offsetEnergy + 0.01 * i for i in range(150)]
#    
# wideEFix = energyToeV * 1**2/4**2 ##because lattice constant is 4nm instead of 1nm
# energiesIneV = np.array(energies)*wideEFix
#    
#    
# 
# for N in range (len(sigmas)):
#     syst = make_wire(l = l,w = w,t = t)
#     sigma = sigmas[N]
#     print('sigma = '+'{:.3e}'.format(sigma))
#     testSys= add_lead(syst, l, w, t, potentialList = potentials, sigma = sigma)
#     #kwant.plot(testSys, site_size=plotSiteSize, file=fileLoc+'IMG.png')
#     testSys=testSys.finalized()
#     data = []
#     data.clear()
#     for energy in energies:
#         #print('energy = '+str(energy))
#         smatrix = kwant.smatrix(testSys, energy)
#         data.append(smatrix.transmission(1, 0))
#     
#     saves = [data, energiesIneV, sigma]
#     np.save(fileLoc+'run'+str(N+8)+'.npy', saves)
#     
# 
# 
# energies = [-offsetEnergy + 0.002 * i for i in range(500)]
# sigmas=np.array([0, 4e11, 1e12, 5e12, 1e13, 5e13, 1e14])*elementary_charge
# 
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/newstuff1nm/wideWireSmall'
# 
# for N in range (len(sigmas)):
#     syst = make_wire(l = l,w = w,t = t)
#     sigma = sigmas[N]
#     print('sigma = '+'{:.3e}'.format(sigma))
#     testSys= add_lead(syst, l, w, t, potentialList = potentials, sigma = sigma)
#     #kwant.plot(testSys, site_size=plotSiteSize, file=fileLoc+'IMG.png')
#     testSys=testSys.finalized()
#     data = []
#     data.clear()
#     for energy in energies:
#         #print('energy = '+str(energy))
#         smatrix = kwant.smatrix(testSys, energy)
#         data.append(smatrix.transmission(1, 0))
#     
#     saves = [data, energiesIneV, sigma]
#     np.save(fileLoc+'run'+str(N+8)+'.npy', saves)
#===============================================================================





#===============================================================================
# ###runs showing effect of dopant location in regular wire - max dist here is 5 from surface
# l = 70
# w = 24
# t = 12
# plotSiteSize = 0.45
# offsetEnergy = 1.95
# datas = []
# Nroughnesses = []
# Nroughness = 0
# energies=[-offsetEnergy + 0.003 * i for i in range(240)]
# energiesIneV = np.array(energies) * energyToeV
# sigma = 1 ##meaningless, just =1 for calculation
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/newstuff1nm/dopantLocation'
# 
# dists = [0,1,2,3,4,5]
# nums = [1,2,3]
# 
# for n in range(len(dists)):
#     datas = []
#     datas.clear()
#     for j in range(len(nums)):
#         syst = make_wire(l = l,w = w,t = t)
#         Ndopant, dopantCharges = createDopantCharges(syst, l, numCharges = nums[j], surfaceDist = dists[n])
#         potentials = calculatePotentials(syst, dopantCharges)
#                
#         testSys= add_lead(syst, l, w, t, potentialList = potentials, sigma = sigma)
#         testSys=testSys.finalized()
#         data = []
#         data.clear()
#         for energy in energies:
#             #print('energy = '+str(energy))
#             smatrix = kwant.smatrix(testSys, energy)
#             data.append(smatrix.transmission(1, 0))
#         datas.append(data)
#         saves = [datas, energiesIneV, nums, dists[n]]
#         np.save(fileLoc+'dist'+str(n)+'.npy', saves)
#===============================================================================










 
#### data-combining surface charge runs
l = 70
w = 24
t = 12
offsetEnergy = 1.95
energies=[-offsetEnergy + 0.0002 * i for i in range(2400)]
energiesIneV = np.array(energies) * energyToeV
   
firstStepeVRanges = [[-2.4058, -2.4035], [-2.4056, -2.401], [-2.404, -2.399], [-2.3865, -2.382], [-2.367, -2.362], [-2.254, -2.249], [-2.157, -2.1525]] ##read off the plot, ordered in increasing sigma for sigma > 0
   
   
syst = make_wire(l = l,w = w,t = t) ###runs with increasing surface charge
sigmas=np.array([0, 1e11, 5e11, 1e12, 5e12, 1e13, 5e13, 1e14])*elementary_charge
#sigmas=np.array([1e11, 5e11, 1e12, 5e12, 1e13, 5e13, 1e14])*elementary_charge
datas = []
datas.clear()
data = []
data.clear()
fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/newstuff1nm/finalSC/SCsrand'
   
if(os.path.isfile( fileLoc+'charges.npy' )):
    print('Loading previous charge distribution...')
    surfaceCharges = np.load(fileLoc+'charges.npy', allow_pickle = True)##use charges already created, so that the distribution is the same in case I want to rerun something
       
else: ##create charges, and save them
    surfaceCharges = createSurfaceCharges(syst, l)
    np.save(fileLoc+'charges.npy', surfaceCharges)
   
potentials = calculatePotentials(syst, surfaceCharges)
 
for N in range (len(sigmas)):
       
    #===========================================================================
    # energiesIneV = np.linspace( firstStepeVRanges[N][0], firstStepeVRanges[N][1], 150 )
    # energies = (energiesIneV / energyToeV).tolist() ##put it back to a list and in units of t
    #===========================================================================
       
       
    syst = make_wire(l = l,w = w,t = t)
    sigma = sigmas[N]
    print('sigma = '+'{:.3e}'.format(sigma))
       
       
    loads = []
    loads.clear()
    if(os.path.isfile( fileLoc+'{:.3e}'.format(sigma)+'run.npy' )): ##file already exists with data for this sigma value
        print('Loading previous data...')
        loads = np.load(fileLoc+'{:.3e}'.format(sigma)+'run.npy', allow_pickle = True) ##load data to combine and avoid calculating duplicate values
           
    else:
        pass
       
       
    testSys= add_lead(syst, l, w, t, potentialList = potentials, sigma = sigma)
    testSys=testSys.finalized()
    data = []
    data.clear()
    usedEs = []
    usedEs.clear()
       
    calcST = time.time() ##start of conductance calculations, to estimate remaining time
    calcingTime = 0
    pctThrs = 5 ##percentage threshold for printing calculation time stuff
       
    for energy in energies:
        eVenergy = energy*energyToeV
           
        if(len(loads) == 0) or not np.any(loads[1] == eVenergy): ##should pass this check if load-list is empty or energy value not already used, and so no values to compare
            #print('energy = '+str(energy))
            smatrix = kwant.smatrix(testSys, energy)
            data.append(smatrix.transmission(1, 0))
            usedEs.append(eVenergy)
           
   
           
        pct = (energies.index(energy) + 1)/len(energies) ## rough fraction of calculations completed
        if( pct >= (pctThrs/100) and verbose): ##every some percentage done print estimated time remaining
            pctThrs = pctThrs + 5
            prevCalcingTime = calcingTime
            calcingTime = time.time() - calcST
            print('{:.2f}'.format(pct*100)+'% complete in '+ '{:.2f}'.format(calcingTime/3600) +' hours.''. Estimated time remaining: '+ '{:.2f}'.format( calcingTime/3600 * (1 - pct)/pct ) +'/'+ '{:.2f}'.format( (calcingTime - prevCalcingTime)/3600 * (1-pct)/(5/100) )+' hours.') ##as it turns out, this is not even close to accurate. the calculation time of a run seems to increase with energy first value is total average rate, second is local... second value tends to be better
               
       
    ##after run re-sort values for saving, if values were previously loaded
    if(os.path.isfile( fileLoc+'{:.3e}'.format(sigma)+'run.npy' )): ##file already exists with data for this sigma value
        print('Sorting run data...')
   
        saveData = np.hstack( (loads[0], np.array(data)) ) ##concatenates the arrays of conductance data
        saveEnergies = np.hstack( (loads[1], np.array(usedEs)) ) ##concatenates the arrays of energy data
           
        sD = np.transpose( np.vstack( (saveData, saveEnergies) ) )
        sD = sD[sD[:, 1].argsort()] ##should sort the data by the second column, saveEneries
        sD = np.transpose(sD)
        saves = [sD[0], sD[1], sigma]
        print('Loaded data length: '+str(len(loads[0]))+', saved data length: '+str(len(sD[0])))
   
    else:
        saves = [np.array(data), energiesIneV, sigma]
           
           
    np.save(fileLoc+'{:.3e}'.format(sigma)+'run.npy', saves)
    print('Run saved for '+'sigma = '+'{:.3e}'.format(sigma)+'.')



  
#===============================================================================
# #### data-combining surface roughness runs
# l = 70
# w = 24
# t = 12
# offsetEnergy = 1.95
# energies=[-offsetEnergy + 0.0002 * i for i in range(2400)]
# energiesIneV = np.array(energies) * energyToeV
#   
# #firstStepeVRanges = [[-2.4058, -2.4035], [-2.4056, -2.401], [-2.404, -2.399], [-2.3865, -2.382], [-2.367, -2.362], [-2.254, -2.249], [-2.157, -2.1525]] ##read off the plot, ordered in increasing sigma for sigma > 0
#  
#  
# NroughnessesAll=[2, 4, 7, 14, 28, 47, 104, 232, 430] ## nums of roughness added to data
# Nroughnesses=[7, 14, 28, 47, 104, 232, 430] ## nums of roughness added to data
# datas = []
# datas.clear()
# data = []
# data.clear()
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/newstuff1nm/finalSC/SR'
#  
#  
#  
# for N in range (len(Nroughnesses)):
#       
#     Nroughness = Nroughnesses[N]
#       
#     #===========================================================================
#     # energiesIneV = np.linspace( firstStepeVRanges[N][0], firstStepeVRanges[N][1], 150 )
#     # energies = (energiesIneV / energyToeV).tolist() ##put it back to a list and in units of t
#     #===========================================================================
#       
#      
#     if(os.path.isfile( fileLoc+'roughs'+str( NroughnessesAll.index(Nroughness) )+'.npy' )):
#         print('Loading previous roughness distribution...')
#         roughedSites = np.load(fileLoc+'roughs'+str( NroughnessesAll.index(Nroughness) )+'.npy', allow_pickle = True).tolist() ##use charges already created, so that the distribution is the same in case I want to rerun something
#          
#         #=======================================================================
#         # print(roughedSites)
#         # print('asd')
#         # print(roughedSites[0])
#         # print(roughedSites[1])
#         #=======================================================================
#          
#         syst = make_wire(l = l,w = w,t = t)
#         testSys, Ncalcd = add_roughness(syst, l, roughness = .11, specifiedSites = roughedSites)
#           
#     else: ##create the desired number of roughnesses, because this is easier than changing the algorithm to accept a requested number, and save the syst. Do not try to use this as-is and just re-generate until you get the exact Nroughness.
#         print('ERROR: did not load roughness data.') #shouldnt get this unless im trying to get new roughs
#         Ncalcd = -1
#         while(Ncalcd != Nroughness):
#             syst = make_wire(l = l,w = w,t = t)
#             testSys, Ncalcd, roughedSites = add_roughness(syst, l, roughness = .277, returnChanged = True)
#               
#             np.save(fileLoc+'roughs'+str(N)+'.npy', roughedSites)
#           
#   
#     testSys = add_lead(testSys, l, w, t)
#           
#     #kwant.plot(testSys, site_size=plotSiteSize)
#       
#     print('N Roughness = '+str(Nroughness))
#       
#       
#     loads = []
#     loads.clear()
#     if(os.path.isfile( fileLoc+'{:.3e}'.format(Nroughness)+'run.npy' )): ##file already exists with data for this sigma value
#         print('Loading previous data...')
#         loads = np.load(fileLoc+'{:.3e}'.format(Nroughness)+'run.npy', allow_pickle = True) ##load data to combine and avoid calculating duplicate values
#           
#     else:
#         pass
#       
#       
#     testSys=testSys.finalized()
#     data = []
#     data.clear()
#     usedEs = []
#     usedEs.clear()
#       
#     calcST = time.time() ##start of conductance calculations, to estimate remaining time
#     calcingTime = 0
#     pctThrs = 5 ##percentage threshold for printing calculation time stuff
#       
#     for energy in energies:
#         eVenergy = energy*energyToeV
#           
#         if(len(loads) == 0) or not np.any(loads[1] == eVenergy): ##should pass this check if load-list is empty or energy value not already used, and so no values to compare
#             #print('energy = '+str(energy))
#             smatrix = kwant.smatrix(testSys, energy)
#             data.append(smatrix.transmission(1, 0))
#             usedEs.append(eVenergy)
#           
#   
#           
#         pct = (energies.index(energy) + 1)/len(energies) ## rough fraction of calculations completed
#         if( pct >= (pctThrs/100) and verbose): ##every some percentage done print estimated time remaining
#             pctThrs = pctThrs + 5
#             prevCalcingTime = calcingTime
#             calcingTime = time.time() - calcST
#             print('{:.2f}'.format(pct*100)+'% complete in '+ '{:.2f}'.format(calcingTime/3600) +' hours.''. Estimated time remaining: '+ '{:.2f}'.format( calcingTime/3600 * (1 - pct)/pct ) +'/'+ '{:.2f}'.format( (calcingTime - prevCalcingTime)/3600 * (1-pct)/(5/100) )+' hours.') ##as it turns out, this is not even close to accurate. the calculation time of a run seems to increase with energy first value is total average rate, second is local... second value tends to be better
#               
#       
#     ##after run re-sort values for saving, if values were previously loaded
#     if(os.path.isfile( fileLoc+'{:.3e}'.format(Nroughness)+'run.npy' )): ##file already exists with data for this sigma value
#         print('Sorting run data...')
#   
#         saveData = np.hstack( (loads[0], np.array(data)) ) ##concatenates the arrays of conductance data
#         saveEnergies = np.hstack( (loads[1], np.array(usedEs)) ) ##concatenates the arrays of energy data
#           
#         sD = np.transpose( np.vstack( (saveData, saveEnergies) ) )
#         sD = sD[sD[:, 1].argsort()] ##should sort the data by the second column, saveEneries
#         sD = np.transpose(sD)
#         saves = [sD[0], sD[1], Nroughness]
#         print('Loaded data length: '+str(len(loads[0]))+', saved data length: '+str(len(sD[0])))
#   
#     else:
#         saves = [np.array(data), energiesIneV, Nroughness]
#           
#           
#     np.save(fileLoc+'{:.3e}'.format(Nroughness)+'run.npy', saves)
#     print('Run saved for Nroughness = '+'{:.3e}'.format(Nroughness)+'.')
#===============================================================================






timeFinish = time.time()
print('Finish, execution time: t= '+ '{:.3e}'.format(timeFinish-timeStart)+' s')