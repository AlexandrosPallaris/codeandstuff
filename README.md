nanowireCubic2.py contains the code that creates and runs the simulations; saved data files and lattice images are saved from here
nanowireCubic2plotting.py contains the code that loads saved data files and plots them; most plots are saved from here
nanowireCubic2extra.py contains code that performs some analysis and plotting; some plots are saved from here


all of these files have use cases below any methods, with old/previous cases commented out in chronological order (bottom of file contains more recent/better code)

I can be contacted at firstnamelastname at yahoo.ca