# By Alexandros Pallaris, for a master's project at Lund University. Last edited: May 24, 2021
##Apologies for the mess.


import kwant
import kwant.continuum
import scipy.sparse.linalg
import scipy.linalg
import numpy as np
import matplotlib as mpl
from matplotlib import pyplot as plt
from random import *
import math
import random
from scipy.constants.constants import epsilon_0, elementary_charge, hbar, m_e, k
import time
import someUtils

pi = math.pi
degtorad = 2*pi/360 #multiply to convert degrees to radians

a = 1e-9  ##lattice constant in nm, defined as equal to one

m_eff = m_e*0.03 ##effective mass.. for In.8Ga.2As
energyToeV = 1*(hbar**2/( 2*m_eff)) / (1e-9)**2 / elementary_charge ##t = hbar^2/(2m^* a^2) = 1, so this converts to eV
energyToJ = 1*(hbar**2/( 2*m_eff)) / (1e-9)**2 ##same but to joules

lat = kwant.lattice.cubic(1)
#lat = kwant.lattice.general([(a, 0, 0), (0, a, 0), (0, 0, a)])



if __name__ == '__main__':###MAIN HERE
    print('Start')
timeStart=time.time()

figsizex = 10
figsizey = 5
linestyles = ['-', '-.', '--']
linewidth = 2

########################
########plot some 
########################


sigmas=[1e9*elementary_charge, 4e9*elementary_charge, 8e9*elementary_charge,  1.2e10*elementary_charge, 3e10*elementary_charge, 6e10*elementary_charge, 1e11*elementary_charge, 3e11*elementary_charge, 6e11*elementary_charge, 1e12*elementary_charge, 5e12*elementary_charge, 1e13*elementary_charge] ##surface charges
fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/definitive_runs_hopefully/surfacechargeruns.npy'
SCDatas = np.load(fileLoc)
 
roughnesses=[.08, .12, .15, .18, .22, .25, .28, .31, .35, .4, .45, .5] ##roughnesses
fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/definitive_runs_hopefully/roughnessruns.npy'
roughnessDatas = np.load(fileLoc)
fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/definitive_runs_hopefully/roughnessrunsNchangedsites.npy'
NSitesChangedDatas = np.load(fileLoc)
 
n = [1e16, 5e16, 1e17, 5e17, 1e18, 3e18, 6e18, 1e19, 5e19, 1e20, 5e20, 1e21] ##doping concentrations
fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/definitive_runs_hopefully/dopingruns.npy'
dopingDatas = np.load(fileLoc)
 
bnesses = [0, .00003, .00006, .0001, .0004, .0008, .0014, .002, .003, .0035, .004, .005, .006, .01] ##bunching values
fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/definitive_runs_hopefully/bunchingruns.npy'
bunchingDatas = np.load(fileLoc)
 
l = 70
w = 24
t = 12
offsetEnergy = 1.95
energies=[-offsetEnergy + 0.0003 * i for i in range(1460)]
energiesIneV = [] #for plotting in different units
for i in range(len(energies)): ##there must be a better way to do this
    energiesIneV.append( energies[i]*energyToeV )
 
 
figsizex = 10
figsizey = 5


#===============================================================================
# for N in range(len(SCDatas)):
#     fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
#     ax1 = fig.add_subplot(111)
#     ax1.plot(energiesIneV, SCDatas[N], label = r'$\sigma = $'+'{:.3e}'.format(sigmas[N]))
#     ax1.set_xlabel('Energy [eV]', fontsize = '15')
#     ax1.set_ylabel(r'conductance [$e^2$/h]', fontsize = '15')
#     ax1.set_title("Band-tail for nanowire with simulated surface charge", fontsize = '17')
#     ax1.grid()
#     fig.tight_layout()
#     plt.legend()
#     plt.savefig('C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/definitive_runs_hopefully/SC'+str(N)+'.png')
#     #plt.show()
#     plt.close()
#      
#      
#     dat = np.log(np.array(SCDatas[N]))
#      
#     fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
#     ax1 = fig.add_subplot(111)
#     ax1.plot(energiesIneV, dat, label = r'$\sigma = $'+'{:.3e}'.format(sigmas[N]))
#     ax1.set_xlabel('Energy [eV]', fontsize = '15')
#     ax1.set_ylabel(r'Log of conductance [$e^2$/h]', fontsize = '15')
#     ax1.set_title("Band-tail for nanowire with simulated surface charge", fontsize = '17')
#     ax1.grid()
#     fig.tight_layout()
#     plt.legend()
#     plt.savefig('C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/definitive_runs_hopefully/SC'+str(N)+'b.png')
#     #plt.show()
#     plt.close()
#      
#      
#      
# for N in range(len(roughnessDatas)):
#     fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
#     ax1 = fig.add_subplot(111)
#     ax1.plot(energiesIneV, roughnessDatas[N], label = 'r = '+'{:.3e}'.format(roughnesses[N])+',N='+str(NSitesChangedDatas[N]))
#     ax1.set_xlabel('Energy [eV]', fontsize = '15')
#     ax1.set_ylabel(r'conductance [$e^2$/h]', fontsize = '15')
#     ax1.set_title("Band-tail for nanowire with simulated roughness", fontsize = '17')
#     ax1.grid()
#     fig.tight_layout()
#     plt.legend()
#     plt.savefig('C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/definitive_runs_hopefully/R'+str(N)+'.png')
#     #plt.show()
#     plt.close()
#      
#      
#     dat = np.log(np.array(roughnessDatas[N]))
#      
#     fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
#     ax1 = fig.add_subplot(111)
#     ax1.plot(energiesIneV, dat, label = 'r = '+'{:.3e}'.format(roughnesses[N])+',N='+str(NSitesChangedDatas[N]))
#     ax1.set_xlabel('Energy [eV]', fontsize = '15')
#     ax1.set_ylabel(r'Log of conductance [$e^2$/h]', fontsize = '15')
#     ax1.set_title("Band-tail for nanowire with simulated roughness", fontsize = '17')
#     ax1.grid()
#     fig.tight_layout()
#     plt.legend()
#     plt.savefig('C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/definitive_runs_hopefully/R'+str(N)+'b.png')
#     #plt.show()
#     plt.close()
#===============================================================================

#===============================================================================
# Ns = [0,3,4,5,6,7,8,9]
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
# for N2 in range(len(Ns)):
#     N = Ns[N2]
#     ax1.plot(energiesIneV, dopingDatas[N], label = 'n = '+'{:.3e}'.format(n[N])+r' $cm^{-3}$', linewidth=linewidth, linestyle = linestyles[N2%len(linestyles)])
#     
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy for nanowire with simulated doping", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig('C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/definitive_runs_hopefully/D'+str(N)+'.png')
# plt.show()
# plt.close()
#===============================================================================
     
     
    #===========================================================================
    # dat = np.log(np.array(dopingDatas[N]))
    #  
    # fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
    # ax1 = fig.add_subplot(111)
    # ax1.plot(energiesIneV, dat, label = r'n = '+'{:.3e}'.format(n[N]))
    # ax1.set_xlabel('Energy [eV]', fontsize = '15')
    # ax1.set_ylabel(r'Log of conductance [$e^2$/h]', fontsize = '15')
    # ax1.set_title("Band-tail for nanowire with simulated doping", fontsize = '17')
    # ax1.grid()
    # fig.tight_layout()
    # plt.legend()
    # plt.savefig('C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/definitive_runs_hopefully/D'+str(N)+'b.png')
    # #plt.show()
    # plt.close()
    #===========================================================================
     
#===============================================================================
# Ns = [0,1,3,5,7,9]
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111) 
# for N2 in range(len(Ns)):
#     N = Ns[N2]
#     ax1.plot(energiesIneV, bunchingDatas[N], label = 'b = '+'{:.3e}'.format(bnesses[N]), linewidth=linewidth, linestyle = linestyles[N2%len(linestyles)])
#     
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy for nanowire with simulated bunching", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig('C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/definitive_runs_hopefully/B'+str(N)+'.png')
# plt.show()
# plt.close()
#===============================================================================
     
     
    #===========================================================================
    # dat = np.log(np.array(bunchingDatas[N]))
    #  
    # fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
    # ax1 = fig.add_subplot(111)
    # ax1.plot(energiesIneV, dat, label = r'b = '+'{:.3e}'.format(bnesses[N]))
    # ax1.set_xlabel('Energy [eV]', fontsize = '15')
    # ax1.set_ylabel(r'Log of conductance [$e^2$/h]', fontsize = '15')
    # ax1.set_title("Band-tail for nanowire with simulated bunching", fontsize = '17')
    # ax1.grid()
    # fig.tight_layout()
    # plt.legend()
    # plt.savefig('C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/definitive_runs_hopefully/B'+str(N)+'b.png')
    # #plt.show()
    # plt.close()
    #===========================================================================




#===============================================================================
# ########################
# ########plot some more
# ########################
#   
# wideEFix = energyToeV * 1**2/4**2 ##because lattice constant is 4nm instead of 1nm
#   
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/widewireruns/'
#   
# l = 25
# w = 250
# t = 5
#   
# figsizex = 10
# figsizey = 5
#   
# energyData = np.array(np.load(fileLoc+'widewirepristinerunEs.npy'))*wideEFix
#   
#   
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
#   
# condData = np.array(np.load(fileLoc+'widewirepristinerun.npy'))
# ax1.plot(energyData, condData)
#       
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Super-wide nanowire with surface charge", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig(fileLoc+'widerunpristine.png')
# plt.show()
# plt.close()
#   
#  
#   
#   
# sigmas=[4e9*elementary_charge, 8e9*elementary_charge,  1.2e10*elementary_charge, 3e10*elementary_charge, 6e10*elementary_charge, 0]
#   
#   
# energyData = np.array(np.load(fileLoc+'widewirelargerangeEs.npy'))*wideEFix
#  
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
#       
# for N in range(6):
#     condData = np.array(np.load(fileLoc+'widewirelargerange'+str(N)+'.npy'))
#   
#     ax1.plot(energyData, condData, label = r'$\sigma$ = '+'{:.3e}'.format(sigmas[N]))
#       
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Super-wide nanowire with surface charge", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig(fileLoc+'wideruna.png')
# plt.show()
# plt.close()
#   
#   
#   
# energyData = np.array(np.load(fileLoc+'widewiresmallrangeEs.npy'))*wideEFix
#   
#   
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
#       
# for N in range(6):
#     condData = np.array(np.load(fileLoc+'widewiresmallrange'+str(N)+'.npy'))
#   
#     ax1.plot(energyData, condData, label = r'$\sigma$ = '+'{:.3e}'.format(sigmas[N]))
#       
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Super-wide nanowire with surface charge", fontsize = '17')
# ax1.set_xlim(-.1375)
# ax1.grid()
# plt.legend()
#   
#   
# ax_inset=fig.add_axes([0.55,0.22,0.4,0.3])
# for N in range(6):
#     condData = np.array(np.load(fileLoc+'widewiresmallrange'+str(N)+'.npy'))
#     ax_inset.plot(energyData, condData)
#   
# ax_inset.set_ylim(18,25.5)
# ax_inset.set_xlim(-.1331, -.128)
#   
# fig.tight_layout()
#   
# plt.savefig(fileLoc+'widerunb.png')
# plt.show()
# plt.close()
#===============================================================================







#===============================================================================
# ########################
# ########plot some log currents
# ########################
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/definitive_runs_hopefully/'
# 
# sigmas=[1e9*elementary_charge, 4e9*elementary_charge, 8e9*elementary_charge,  1.2e10*elementary_charge, 3e10*elementary_charge, 6e10*elementary_charge, 1e11*elementary_charge, 3e11*elementary_charge, 6e11*elementary_charge, 1e12*elementary_charge, 5e12*elementary_charge, 1e13*elementary_charge] ##surface charges
# Ts = [1e-12, 1e-2, 1, 10, 100, 400]
# offsetEnergy = 1.95
# Vs = np.array([offsetEnergy -0.1 + 0.0003 * i for i in range(800)]) ###in Volts
# 
# for t in range(len(Ts)):
#     
#     for n in range(len(sigmas)):
#         current = np.load(fileLoc+'surfacechargecurrents'+str(n)+'T'+str(t)+'.npy')
#         
#         fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
#         ax1 = fig.add_subplot(111)
#         ax1.plot(Vs, np.log(np.array(current)), label = r'$\sigma = $'+'{:.3e}'.format(sigmas[n]))
#         ax1.set_xlabel('Voltage [V]', fontsize = '15')
#         ax1.set_ylabel(r'Log of I [$e^2$/h]', fontsize = '15')
#         ax1.set_title("ln(I) by V for nanowire with simulated surface charge", fontsize = '17')
#         ax1.grid()
#         fig.tight_layout()
#         plt.legend()
#         plt.savefig(fileLoc+'SCIlog'+str(n)+'T'+str(t)+'.png')
#         #plt.show()
#         plt.close()
#===============================================================================


########################
########plot some new stuff
########################
fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/newstuff/'


figsizex = 10
figsizey = 5

#===============================================================================
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
# 
# sigmas=[0, 1e11*elementary_charge, 1e12*elementary_charge, 1e13*elementary_charge, 1e14*elementary_charge, 1e15*elementary_charge]
# 
# 
# datas = np.load(fileLoc+'SCsreg.npy', allow_pickle = True)
# for n in range(len(datas[0])):
#     ax1.plot(datas[1], datas[0][n], label = r'$\sigma = $'+'{:.3e}'.format(sigmas[n]))
#     
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy nanowire with regular simulated surface charge", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig(fileLoc+'imgReg.png')
# plt.show()
# plt.close()
#===============================================================================


#===============================================================================
# datas = np.load(fileLoc+'overallDoping.npy', allow_pickle = True)
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
# for n in range(len(datas[2])):
#     ax1.plot(datas[1], datas[0][n], label = r'$overall effect = $'+'{:.3e}'.format(datas[2][n]), linewidth=linewidth, linestyle = linestyles[n%len(linestyles)])
#         
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy for nanowire with positive overall doping effect", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig(fileLoc+'imgDopOverall.png')
# plt.show()
# plt.close()
#    
# datas = np.load(fileLoc+'overallDopingNeg.npy', allow_pickle = True)
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
# for n in range(len(datas[2])):
#     ax1.plot(datas[1], datas[0][n], label = r'$overall effect = $'+'{:.3e}'.format(datas[2][n]), linewidth=linewidth, linestyle = linestyles[n%len(linestyles)])
#         
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy for nanowire with negative overall doping effect", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig(fileLoc+'imgDopOverallNeg.png')
# plt.show()
# plt.close()    
#===============================================================================




 
 
#===============================================================================
# datas = np.load(fileLoc+'SCcsrnlngths.npy', allow_pickle = True)
# datas2 = []
# for i in range(len(datas[3])):
#     datas2.append(np.load(fileLoc+'SCcsrnlngthscondata'+str(i)+'.npy', allow_pickle = True))
#     
#     
# #===============================================================================
# # for j in range(len(datas[3])):
# #     fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# #     ax1 = fig.add_subplot(111)
# #     for n in range(len(datas[2])):
# #         ax1.plot(datas[1], datas2[j][n], label = r'$\sigma = $'+'{:.3e}'.format(datas[2][n]))
# #       
# #     ax1.set_xlabel('Energy [eV]', fontsize = '15')
# #     ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# #     ax1.set_title(r"Conductance by energy nanowire: surface charges with $l_s$ = "+'{:.3e}'.format(datas[3][j]), fontsize = '17')
# #     ax1.grid()
# #     fig.tight_layout()
# #     plt.legend()
# #     plt.savefig(fileLoc+'imgscrnlngth'+str(j)+'.png')
# #     plt.show()
# #     plt.close()
# #===============================================================================
#         
# Ns = [2,3,4,5,6,7]
# for n in range(len(datas[2])):
#     fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
#     ax1 = fig.add_subplot(111)
#     for N2 in range(len(Ns)):
#         j = Ns[N2]
#         ax1.plot(datas[1], datas2[j][n], label = r'$\lambda_s = $'+'{:.3e}'.format(1e9*datas[3][j])+' nm', linewidth=linewidth, linestyle = linestyles[N2%len(linestyles)])
#         
#     ax1.set_xlabel('Energy [eV]', fontsize = '15')
#     ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
#     ax1.set_title(r"Conductance by energy for nanowire with surface charge of $\sigma$ = "+'{:.3e}'.format(datas[2][n])+' C/cm$^{2}$', fontsize = '16')
#     ax1.grid()
#     fig.tight_layout()
#     plt.legend()
#     plt.savefig(fileLoc+'imgscrnlngthsigs'+str(n)+'.png')
#     plt.show()
#     plt.close()
#===============================================================================
     
    
    







#===============================================================================
# datas = np.load(fileLoc+'connectboxsize0.2.npy', allow_pickle = True)
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
# for n in range(len(datas[2])):
#     ax1.plot(datas[1], datas[0][n], label = '$size scaling factor = $'+str(datas[2][n]))
#      
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy for nanowire with box leads", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig(fileLoc+'connectboxsize0.2plots.png')
# plt.show()
# plt.close()
#  
# datas = np.load(fileLoc+'smallerwireconnectboxsize0.2.npy', allow_pickle = True)
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
# for n in range(len(datas[2])):
#     ax1.plot(datas[1], datas[0][n], label = '$size scaling factor = $'+str(datas[2][n]))
#      
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy for small nanowire with box leads", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig(fileLoc+'smallerwireconnectboxsize0.2plots.png')
# plt.show()
# plt.close()
#  
# datas = np.load(fileLoc+'largerwireconnectboxsize0.2.npy', allow_pickle = True)
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
# for n in range(len(datas[2])):
#     ax1.plot(datas[1], datas[0][n], label = '$size scaling factor = $'+str(datas[2][n]))
#      
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy for large nanowire with box leads", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig(fileLoc+'largerwireconnectboxsize0.2plots.png')
# plt.show()
# plt.close()
# 
# datas = np.load(fileLoc+'connectcornersize0.2.npy', allow_pickle = True)
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
# for n in range(len(datas[2])):
#     ax1.plot(datas[1], datas[0][n], label = '$size scaling factor = $'+str(datas[2][n]))
#      
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy for nanowire with corner leads", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig(fileLoc+'connectcornersize0.2plots.png')
# plt.show()
# plt.close()
#  
# datas = np.load(fileLoc+'smallerwireconnectcornersize0.2.npy', allow_pickle = True)
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
# for n in range(len(datas[2])):
#     ax1.plot(datas[1], datas[0][n], label = '$size scaling factor = $'+str(datas[2][n]))
#      
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy for small nanowire with corner leads", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig(fileLoc+'smallerwireconnectcornersize0.2plots.png')
# plt.show()
# plt.close()
#  
# datas = np.load(fileLoc+'largerwireconnectcornersize0.2.npy', allow_pickle = True)
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
# for n in range(len(datas[2])):
#     ax1.plot(datas[1], datas[0][n], label = '$size scaling factor = $'+str(datas[2][n]))
#      
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy for large nanowire with corner leads", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig(fileLoc+'largerwireconnectcornersize0.2plots.png')
# plt.show()
# plt.close()
#===============================================================================










#===============================================================================
# datas = np.load(fileLoc+'SCsneg.npy', allow_pickle = True)
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
# 
# 
# for n in range(len(datas[0])):
#     ax1.plot(datas[1], datas[0][n], label = r'$\sigma = $'+'{:.3e}'.format(sigmas[n]))
#     
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy nanowire with negative simulated surface charge", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig(fileLoc+'imgNeg.png')
# plt.show()
# plt.close()
#===============================================================================


#===============================================================================
# datas = np.load(fileLoc+'SCspos.npy', allow_pickle = True)
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
# 
# 
# 
# for n in range(len(datas[0])):
#     ax1.plot(datas[1], datas[0][n], label = r'$\sigma = $'+'{:.3e}'.format(sigmas[n]))
#     
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy nanowire with positive simulated surface charge", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig(fileLoc+'imgPos.png')
# plt.show()
# plt.close()
#===============================================================================




#===============================================================================
# datas = np.load(fileLoc+'connectcorner3.npy', allow_pickle = True)
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
#  
#  
# ax1.plot(datas[1], datas[0])
#      
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy nanowire with a 'corner' connection", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig(fileLoc+'imgcorner3.png')
# plt.show()
# plt.close()
#===============================================================================



#===============================================================================
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
#    
# datas = np.load(fileLoc+'connectbox.npy', allow_pickle = True)
# ax1.plot(datas[1], datas[0], label = "Nanowire with a rectangular connection", linewidth=linewidth, linestyle = linestyles[0%len(linestyles)])
# datas = np.load(fileLoc+'connectcorner.npy', allow_pickle = True)
# ax1.plot(datas[1], datas[0], label = "Nanowire with a rectangular corner connection", linewidth=linewidth, linestyle = linestyles[1%len(linestyles)])
# datas = np.load(fileLoc+'connectcorner2.npy', allow_pickle = True)
# ax1.plot(datas[1], datas[0], label = "Nanowire with a flat-backed rectangular corner connection", linewidth=linewidth, linestyle = linestyles[2%len(linestyles)])
# datas = np.load(fileLoc+'connectcorner3.npy', allow_pickle = True)
# ax1.plot(datas[1], datas[0], label = "Nanowire with a flat-backed ovular corner connection", linewidth=linewidth, linestyle = linestyles[3%len(linestyles)])
#      
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy for nanowires with various lead connections", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig(fileLoc+'imgbox.png')
# plt.show()
# plt.close()
#===============================================================================

#===============================================================================
# 
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
#  
# datas = np.load(fileLoc+'connectpot.02.npy', allow_pickle = True)
# ax1.plot(datas[1], datas[0], label = 'V = .02 t', linewidth=linewidth, linestyle = linestyles[0%len(linestyles)])
# 
# datas = np.load(fileLoc+'connectpot.1.npy', allow_pickle = True)
# ax1.plot(datas[1], datas[0], linewidth=linewidth, label = 'V = .1 t', linestyle = linestyles[1%len(linestyles)])
# 
# datas = np.load(fileLoc+'connectpot.3.npy', allow_pickle = True)
# ax1.plot(datas[1], datas[0], linewidth=linewidth, label = 'V = .3 t', linestyle = linestyles[2%len(linestyles)])
#  
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy for nanowire with a potential barrier connection", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig(fileLoc+'imgpotthree.png')
# plt.show()
# plt.close()
#===============================================================================

#===============================================================================
# datas = np.load(fileLoc+'connectsimple.npy', allow_pickle = True)
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
# 
# 
# ax1.plot(datas[1], datas[0])
#     
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy nanowire with a simple connection", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig(fileLoc+'imgsimple.png')
# plt.show()
# plt.close()
#===============================================================================





fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/newstuff1nm/'



 
Ntotal = 13020
nChoices = [0,2,4,6,8,10,12]

#===============================================================================
# 
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
# datas = np.load(fileLoc+'inDopesPos.npy', allow_pickle = True)
# 
#  
# for n in nChoices:
#     ax1.plot(datas[1], datas[0][n], label = r'$Ndopants = $'+str(datas[2][n])+', n = '+'{:.3e}'.format(datas[2][n]/Ntotal*(1e-2/1e-9)**3)+r' $cm^{-3}$', linewidth=linewidth, linestyle = linestyles[n%len(linestyles)])
#           
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy for nanowire with positive dopant charge", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig(fileLoc+'imgDopChgPos.png')
# plt.show()
# plt.close()
#       
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
# datas = np.load(fileLoc+'inDopesNeg.npy', allow_pickle = True)
# for n in nChoices:
#     ax1.plot(datas[1], datas[0][n], label = r'$Ndopants = $'+str(datas[2][n])+', n = '+'{:.3e}'.format(datas[2][n]/Ntotal*(1e-2/1e-9)**3)+r' $cm^{-3}$', linewidth=linewidth, linestyle = linestyles[n%len(linestyles)])
#           
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy for nanowire with negative dopant charge", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig(fileLoc+'imgDopChgNeg.png')
# plt.show()
# plt.close()
#===============================================================================


#===============================================================================
# mpl.rcParams['xtick.labelsize'] = 12
# mpl.rcParams['ytick.labelsize'] = 12
# 
# datasn = np.load(fileLoc+'inDopesNeg.npy', allow_pickle = True)
# datasp = np.load(fileLoc+'inDopesPos.npy', allow_pickle = True)
# 
# fig = plt.figure( figsize=(figsizex*1.7, figsizey*1.2), dpi=80, facecolor='w', edgecolor='k')
# ax = fig.add_subplot(111, frameon=False)
# ax1 = fig.add_subplot(121)
# ax2 = fig.add_subplot(122)
# ax.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
# 
# 
# 
# for n in nChoices:
#     ax2.plot(datasn[1], datasn[0][n])
#     
# for n in nChoices:
#     ax1.plot(datasp[1], datasp[0][n], label = r'$Ndopants = $'+str(datasp[2][n])+', n = '+'{:.3e}'.format(datasp[2][n]/Ntotal*(1e-2/1e-9)**3)+r' $cm^{-3}$', linewidth=linewidth, linestyle = linestyles[n%len(linestyles)])
#          
# ax.set_xlabel('Energy [eV]', fontsize = '17')
# ax1.set_title('+e Dopant Charges', fontsize = '17')
# ax2.set_title('-e Dopant Charge', fontsize = '17')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '17')
# fig.suptitle("Conductance by energy for nanowire with simulated dopant charge", fontsize = '20')
# ax1.grid()
# ax2.grid()
# fig.tight_layout()
# ax1.legend(fontsize = '10')
# plt.savefig(fileLoc+'imgDopPosNeg.png')
# plt.show()
# plt.close()
#===============================================================================

 
 
 
 
 
  
Ntotal = 36244  ###total sites eligible for doping here
    
#===============================================================================
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
# datas = np.load(fileLoc+'connectcornerdopedpos.npy', allow_pickle = True)
# for n in range(len(datas[2])):
#     ax1.plot(datas[1], datas[0][n], label = r'$Ndopants = $'+str(datas[2][n])+', n = '+'{:.3e}'.format(datas[2][n]/Ntotal*(1e-2/1e-9)**3)+r' $cm^{-3}$')
#         
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy - nanowire with doped corner connection", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig(fileLoc+'imgCornerDopePos.png')
# plt.show()
# plt.close()
#===============================================================================
 
 
 
#===============================================================================
# Ns = [0, 3, 6, 9, 12, 15]
#  
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
# datas = np.load(fileLoc+'connectcornerdopedpos.npy', allow_pickle = True)
# for n in Ns:
#     ax1.plot(datas[1], datas[0][n], label = r'$Ndopants = $'+str(datas[2][n])+', n = '+'{:.3e}'.format(datas[2][n]/Ntotal*(1e-2/1e-9)**3)+r' $cm^{-3}$')
#         
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy - nanowire with doped corner connection", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig(fileLoc+'imgCornerDopePos.png')
# plt.show()
# plt.close()
#===============================================================================
    
 
 
  
#===============================================================================
# Ntotal = 27880  ###total sites eligible for doping here
#      
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
# datas = np.load(fileLoc+'connectcornerdopedpos2.npy', allow_pickle = True)
# for n in range(len(datas[2])):
#     ax1.plot(datas[1], datas[0][n], label = r'$Ndopants = $'+str(datas[2][n])+', n = '+'{:.3e}'.format(datas[2][n]/Ntotal*(1e-2/1e-9)**3)+r' $cm^{-3}$')
#           
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy - nanowire with doped corner connection", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig(fileLoc+'imgCornerDopePos2.png')
# plt.show()
# plt.close()
#  
#  
#   
# Ns = [0, 2, 5, 8, 11, 13]
#       
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
# datas = np.load(fileLoc+'connectcornerdopedpos2.npy', allow_pickle = True)
# for N in range(len(Ns)):
#     n = Ns[N]
#     ax1.plot(datas[1], datas[0][n], label = r'$Ndopants = $'+str(datas[2][n])+', n = '+'{:.3e}'.format(datas[2][n]/Ntotal*(1e-2/1e-9)**3)+r' $cm^{-3}$', linewidth=linewidth, linestyle = linestyles[N%len(linestyles)])
#          
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy for nanowire with doped corner connection", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig(fileLoc+'imgCornerDopePos2.png')
# plt.show()
# plt.close()
#===============================================================================



      
#===============================================================================
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
# datas = np.load(fileLoc+'connectcornerdopedneg.npy', allow_pickle = True)
# Ns = [0, 2, 5, 8, 11, 14]
# #Ns = np.arange(len(datas[0]))
# for N in range(len(Ns)):
#     n = Ns[N]
#     ax1.plot(datas[1], datas[0][n], label = r'$Ndopants = $'+str(datas[2][n])+', n = '+'{:.3e}'.format(datas[2][n]/Ntotal*(1e-2/1e-9)**3)+r' $cm^{-3}$', linewidth=linewidth, linestyle = linestyles[N%len(linestyles)])
#          
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy for nanowire with n-doped corner connection", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig(fileLoc+'imgCornerDopeNeg.png')
# plt.show()
# plt.close()
#===============================================================================









#===============================================================================
# ##some placed dopant charge runs
# l = 70
# w = 24
# t = 12
#  
# dists = [0,1,2,3,4,5]
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/newstuff1nm/dopantLocation'
#  
# pristineData = np.load('C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/newstuff/connectsimple.npy', allow_pickle = True)
#  
#  
# for j in range(3):
#     fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
#     ax1 = fig.add_subplot(111)
#      
#     ax1.plot(pristineData[1], pristineData[0], label = 'no dopants')
#     for N in range(len(dists)):
#         datas = np.load(fileLoc+'dist'+str(N)+'.npy', allow_pickle = True)
#         ax1.plot(datas[1], datas[0][j], label = 'dopants '+str(datas[3])+' nm from surface', linewidth=linewidth, linestyle = linestyles[(N+1)%len(linestyles)])
#      
#          
#     ax1.set_xlabel('Energy [eV]', fontsize = '15')
#     ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
#     ax1.set_title("Conductance by energy - nanowire with "+str(datas[2][j])+" placed dopant charges", fontsize = '17')
#     ax1.grid()
#     fig.tight_layout()
#     plt.legend()
#     plt.savefig(fileLoc+'surfaceChargeNums'+str(j)+'.png')
#     plt.show()
#     plt.close()
#  
#  
# for N in range(len(dists)):
#     fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
#     ax1 = fig.add_subplot(111)
#     datas = np.load(fileLoc+'dist'+str(N)+'.npy', allow_pickle = True)
#      
#     ax1.plot(pristineData[1], pristineData[0], label = 'no dopants')
#     for j in range(len(datas[2])):
#         ax1.plot(datas[1], datas[0][j], label = str(datas[2][j])+' dopants', linewidth=linewidth, linestyle = linestyles[(j+1)%len(linestyles)])
#          
#     ax1.set_xlabel('Energy [eV]', fontsize = '15')
#     ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
#     ax1.set_title("Conductance by energy - nanowire with dopant charge "+str(datas[3])+" nm from wire surface", fontsize = '17')
#     ax1.grid()
#     fig.tight_layout()
#     plt.legend()
#     plt.savefig(fileLoc+'surfaceChargePlcd'+str(N)+'.png')
#     plt.show()
#     plt.close()
#===============================================================================


 
##some more wide-wire runs
l = 25
w = 250
t = 5
    
sigmas=np.array([0, 1e11, 1e12, 1e13, 1e14, 1e15, 1e16, 1e17])*elementary_charge ##RUN1
fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/newstuff1nm/wideWire'
    
    
#===============================================================================
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
#     
# for N in range(len(sigmas)):
#     data = np.load(fileLoc+'run'+str(N+8)+'.npy', allow_pickle = True)
#     ax1.plot(data[1], data[0], label = r'$\sigma = $'+'{:.3e}'.format(data[2])+r' $C/cm^{2}$', linewidth=linewidth, linestyle = linestyles[N%len(linestyles)])
#             
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy - wide nanowire with surface charge", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig(fileLoc+'surfaceCharge.png')
# plt.show()
# plt.close()
#===============================================================================


#===============================================================================
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
#        
# for N in range(len(sigmas)):
#     data = np.load(fileLoc+'run'+str(N+8)+'.npy', allow_pickle = True)
#     ax1.plot(data[1], data[0], label = r'$\sigma = $'+'{:.3e}'.format(data[2])+r' $C/cm^{2}$', linewidth=linewidth, linestyle = linestyles[N%len(linestyles)])
#  
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy for wide nanowire with surface charge", fontsize = '17')
# ax1.set_xlim(-.1375, -.092)
# ax1.grid()
# plt.legend()
#    
#    
# ax_inset=fig.add_axes([0.55,0.17,0.4,0.28])
# for N in range(len(sigmas)):
#     data = np.load(fileLoc+'run'+str(N+8)+'.npy', allow_pickle = True)
#     ax_inset.plot(data[1], data[0], linewidth=linewidth, linestyle = linestyles[N%len(linestyles)])
#    
# ax_inset.set_ylim(32.5,38.5)
# ax_inset.set_xlim(-.1207, -.118)
#    
# fig.tight_layout()
#    
# plt.savefig(fileLoc+'wideruninset.png')
# plt.show()
# plt.close()
#===============================================================================



#===============================================================================
# wideEFix = energyToeV * 1**2/4**2 ##because lattice constant is 4nm instead of 1nm
#    
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/widewireruns/'
#    
# l = 25
# w = 250
# t = 5
#    
# 
# energyData = np.array(np.load(fileLoc+'widewirepristinerunEs.npy'))*wideEFix
#    
#    
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
#    
# condData = np.array(np.load(fileLoc+'widewirepristinerun.npy'))
# ax1.plot(energyData, condData, linewidth=linewidth)
#        
#        
# ax_inset=fig.add_axes([0.12,0.487,0.4,0.38])
# 
# ax_inset.plot(energyData, condData, linewidth=linewidth)
#    
# ax_inset.set_ylim(65, 89)
# ax_inset.set_xlim(-.084, -.076)
#        
# 
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy for wide nanowire", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.savefig(fileLoc+'widerunpristine.png')
# plt.show()
# plt.close()
#===============================================================================








#===============================================================================
# sigmas=np.array([0, 4e11, 1e12, 5e12, 1e13])*elementary_charge ##RUN2
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/newstuff1nm/wideWireSmall'
#     
#     
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
#     
# ##forgot to convert energies for this run
# energies = [-offsetEnergy + 0.002 * i for i in range(500)]
# wideEFix = energyToeV * 1**2/4**2 ##because lattice constant is 4nm instead of 1nm
# energiesIneV = np.array(energies)*wideEFix
#     
# for N in range(len(sigmas)):
#     data = np.load(fileLoc+'run'+str(N+8)+'.npy', allow_pickle = True)
#     ax1.plot(energiesIneV, data[0], label = r'$\sigma = $'+'{:.3e}'.format(data[2])+r' $C/cm^{2}$', linewidth=linewidth, linestyle = linestyles[N%len(linestyles)])
#             
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy - wide nanowire with surface charge", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig(fileLoc+'surfaceCharge.png')
# plt.show()
# plt.close()
#===============================================================================



 
 
#===============================================================================
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/newstuff1nm/wideWireLarge'
# sigmas=np.array([0, 4e11, 1e12, 5e12, 1e13, 5e13, 1e14])*elementary_charge
#   
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
#  
#  
#  
# for N in range(len(sigmas)):
#     data = np.load(fileLoc+'run'+str(8+N)+'.npy', allow_pickle = True)
#     ax1.plot(data[1], data[0], label = r'$\sigma = $'+'{:.3e}'.format(data[2])+r' $C/cm^{2}$', linewidth=linewidth, linestyle = linestyles[N%len(linestyles)])
#             
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy - wide nanowire with surface charge", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig(fileLoc+'surfaceCharge.png')
# plt.show()
# plt.close()
#===============================================================================



  
#===============================================================================
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/newstuff1nm/finalSC/'
#      
# #===============================================================================
# # datas = np.load(fileLoc+'SCsneg.npy', allow_pickle = True)
# #     
# # fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# # ax1 = fig.add_subplot(111)
# #      
# #      
# # for n in range(len(datas[0])):
# #     ax1.plot(datas[1], datas[0][n], label = r'$\sigma = $'+'{:.3e}'.format(datas[2][n])+' $C/cm^2$', linewidth=linewidth, linestyle = linestyles[n%len(linestyles)])
# #          
# # ax1.set_xlabel('Energy [eV]', fontsize = '15')
# # ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# # ax1.set_title("Conductance by energy for nanowire with negative surface charge", fontsize = '17')
# # ax1.grid()
# # fig.tight_layout()
# # plt.legend()
# # plt.savefig(fileLoc+'imgNeg.png')
# # plt.show()
# # plt.close()
# #     
# #     
# # datas = np.load(fileLoc+'SCspos.npy', allow_pickle = True)
# # fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# # ax1 = fig.add_subplot(111)
# #     
# #     
# #     
# # for n in range(len(datas[0])):
# #     ax1.plot(datas[1], datas[0][n], label = r'$\sigma = $'+'{:.3e}'.format(datas[2][n])+' $C/cm^2$', linewidth=linewidth, linestyle = linestyles[n%len(linestyles)])
# #          
# # ax1.set_xlabel('Energy [eV]', fontsize = '15')
# # ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# # ax1.set_title("Conductance by energy nanowire for with positive surface charge", fontsize = '17')
# # ax1.grid()
# # fig.tight_layout()
# # plt.legend()
# # plt.savefig(fileLoc+'imgPos.png')
# # plt.show()
# # plt.close()
# #===============================================================================
#  
# mpl.rcParams['xtick.labelsize'] = 12
# mpl.rcParams['ytick.labelsize'] = 12
#  
# datasn = np.load(fileLoc+'SCsneg.npy', allow_pickle = True)
# datasp = np.load(fileLoc+'SCspos.npy', allow_pickle = True)
#  
# fig = plt.figure( figsize=(figsizex*1.5, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax = fig.add_subplot(111, frameon=False)
# ax1 = fig.add_subplot(121)
# ax2 = fig.add_subplot(122)
# ax.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
#  
# for n in range(len(datasn[0])):
#     ax2.plot(datasn[1], datasn[0][n])
#      
# for n in range(len(datasp[0])):
#     ax1.plot(datasp[1], datasp[0][n], label = r'$\sigma = $'+'{:.3e}'.format(datasp[2][n])+' $C/cm^2$', linewidth=linewidth, linestyle = linestyles[n%len(linestyles)])
#           
# ax.set_xlabel('Energy [eV]', fontsize = '17')
# ax1.set_title('Positive-only Charge', fontsize = '17')
# ax2.set_title('Negative-only Charge', fontsize = '17')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '17')
# fig.suptitle("Conductance by energy for nanowire with surface charge", fontsize = '20')
# ax1.grid()
# ax2.grid()
# fig.tight_layout()
# ax1.legend(fontsize = '10')
# plt.savefig(fileLoc+'imgPosNeg.png')
# plt.show()
# plt.close()
#===============================================================================




#===============================================================================
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/newstuff1nm/finalSC/SCsrand'
# sigmas=np.array([0, 1e11, 5e11, 1e12, 5e12, 1e13, 5e13, 1e14])*elementary_charge
#   
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
#       
#        
# for n in range(len(sigmas)):
#     sigma = sigmas[n]
#     datas = np.load(fileLoc+'{:.3e}'.format(sigma)+'run.npy', allow_pickle = True)
#     ax1.plot(datas[1], datas[0], label = r'$\sigma = $'+'{:.3e}'.format(datas[2])+r' C/cm$^2$', linewidth=linewidth, linestyle = linestyles[n%len(linestyles)])
#       
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy for nanowire with simulated surface charge", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig(fileLoc+'img.png')
# plt.show()
# plt.close()
#===============================================================================




#===============================================================================
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/newstuff1nm/finalSC/SR'
# NroughnessesAll=[2, 4, 7, 14, 28, 47, 104, 232, 430] ## nums of roughness added to data
#  
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
#      
#       
# for n in range(len(NroughnessesAll)):
#     Nroughness = NroughnessesAll[n]
#     datas = np.load(fileLoc+'{:.3e}'.format(Nroughness)+'run.npy', allow_pickle = True)
#     ax1.plot(datas[1], datas[0], label = r'$N_{r} = $'+'{:.0f}'.format(datas[2]) + ' ('+'{:.2e}'.format(datas[2]/3772*(1e-2/1e-9)**2)+' cm$^{-2}$)', linewidth=linewidth, linestyle = linestyles[n%len(linestyles)])
#      
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance by energy for nanowire with surface roughness", fontsize = '17')
# ax1.grid()
# fig.tight_layout()
# plt.legend()
# plt.savefig(fileLoc+'img.png')
# plt.show()
# plt.close()
#===============================================================================




timeFinish = time.time()
print('Finish, execution time: t= '+ '{:.3e}'.format(timeFinish-timeStart)+' s')