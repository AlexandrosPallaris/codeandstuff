# By Alexandros Pallaris, for a master's project at Lund University. Last edited: May 24, 2021
##Apologies for the mess.

import numpy as np
import matplotlib as mpl
from matplotlib import pyplot as plt
import math
from scipy.constants.constants import epsilon_0, elementary_charge, hbar, m_e, Planck
import scipy.optimize

k = 8.617333*10**-5 ###boltzmann constant in eV/K
pi = math.pi
m_eff = 0.03*m_e
T = 1e-12  ###temperature, used for the fermi distribution. I am using 1e-12 for 'zero'
energyToeV = 1*(hbar**2/( 2*m_eff)) / (1e-9)**2 / elementary_charge ##t = hbar^2/(2m^* a^2) = 1, so this converts to eV


def printSquareEnergyGaps(l_x,l_y): ##l in nanometers
    l_x = l_x * 10**-9
    l_y = l_y * 10**-9
    def En(n_x,n_y,l_x,l_y):
        return hbar**2 *pi**2 / ( 2*m_eff ) * (n_x**2 / l_x**2 + n_y**2 / l_y**2  ) / elementary_charge
    
    l=25
    energies = np.zeros(l)
    
    for n_x in range(5):
        for n_y in range(5):
            energies[5*n_x + n_y] = En(n_x+1, n_y+1, l_x, l_y)

    energies = np.sort(energies)
    
    print('energies: ('+str(l_x*10**9)+'x'+str(l_y*10**9)+')')
    print(energies)
    
    
    
def fermi(E): ##should be the fermi distribution
    #print('E/kT = '+str(E/(k*T)))
    return 1 / ( np.exp( E/(k*T) ) + 1 )

def dF(energy, Ef, beta): ##derivative of fermi
    return 1/4 * beta/(np.cosh(beta*(energy-Ef)/2))**2

def currentIntegrand(cond, E, mu): ###integrate conductance to find current
    #print('cond: '+str(cond))
    #print('ferm: '+str(fermi(E)))
    #print('E: '+str(E))
    #print('mu: '+str(mu))
    #return cond*( fermi(E - mu/2) - fermi(E + mu/2) )
    return cond  

def currentIntegrate(condData, energyData, V): ### extemely inefficient integration: conductance over voltage for current using just trapezium method, assume non-interacting system so that voltage only goes into mu
    ans = 0
    for i in range(len(condData) - 1):
        
        if(energyData[i] > V):##a way of enforcing the integration limit, since conductance data should start at zero
            break
        
        h = energyData[i+1] - energyData[i]
        ans = ans + 0.5*h*( currentIntegrand(condData[i], energyData[i], V) + currentIntegrand(condData[i+1], energyData[i+1], V) )
    
    
    return ans *elementary_charge**2 /Planck

def urbTail(x, a, b):
    return a*(x-b)


def Use_temp(trans, energies, inEs, beta): ##integrates to approximate conductance at higher temperatures from fermi broadening
    couple_T_E=list(zip(trans,energies))    # here, I have a list of (T,E)

    Conductance=[]
    Fermi_E= inEs    #Here I chose the energies of the Fermi Dirac in the same ensemble as the one of the transmission
    #this way I do not have to recalculate the transmissions again.

    for Ef in Fermi_E:
        Tr=[T*dF(energy,Ef=Ef,beta=beta) for T,energy in couple_T_E]
        np.trapz
        g=np.trapz(Tr, energies)#,dx=1/(15*beta))  # this is the conductance at one energy.
        Conductance.append(g)
    return Conductance, Fermi_E,trans,energies


if __name__ == '__main__':###MAIN HERE
    print('Start')


#===============================================================================
# Es20x20 = np.array([-2.400, -2.317, -2.260, -2.175, -2.07, -2.04, -1.985, -1.848, -1.833, -1.75, -1.655, -1.61, -1.555])##energies of conductance steps from kwant simulation of a 20x20 nanowire. start at step of conductance 1-2, since the offset wan't high enough to see 0-1. whoops.
# Es10x10 = np.array([-2.336, -2.033, -1.733, -1.561, -1.26, -0.95, -0.79])##energies of conductance steps from kwant simulation of a 10x10 nanowire. start at step of conductance 0-1
# fac = 1.155 ##for 20x20
# printSquareEnergyGaps(l_x = 20, l_y = 20)
# print((Es20x20 - Es20x20[0]+0.1566/fac)*fac)
# fac = 1.25 ###for 10x10
# printSquareEnergyGaps(l_x = 10, l_y = 10)
# print((Es10x10 - Es10x10[0]+0.2507/fac)*fac)
#===============================================================================


#===============================================================================
# fac = hbar**2 /2 /m_eff / elementary_charge * 10**18
# 
# Es15x15 = np.array([-1.9236, -1.8095, -1.6955, -1.6244])##energies of conductance steps from kwant simulation of a 15x15 nanowire - not unit converted. start at step of conductance 0-1
# printSquareEnergyGaps(15, 15)
# print((Es15x15 - Es15x15[0])*fac + 0.1114)
# fac15 = (.44566-0.1114)/(.40108-0.1114)
# 
# Es5x5 = np.array([-1.465, -0.733, -0.001, 0.267])##energies of conductance steps from kwant simulation of a 5x5 nanowire - not unit converted. start at step of conductance 0-1
# printSquareEnergyGaps(5, 5)
# print((Es5x5 - Es5x5[0])*fac + 1.002747)
# fac5 = (4.0109-1.0027)/(3.20237-1.0027)
# 
# 
# Es10x10 = np.array([-1.837, -1.601, -1.365, -1.229])##10x10
# printSquareEnergyGaps(10, 10)
# print((Es10x10 - Es10x10[0])*fac + 0.250686)
# fac10 = (1.0027-0.250686)/(0.850123-0.250686)
# 
# Es20x20 = np.array([-1.955, -1.899, -1.823, -1.779, -1.713, -1.631, -1.603, -1.563])##20x20 again
# printSquareEnergyGaps(20, 20)
# print((Es20x20 - Es20x20[0])*fac + 0.06267)
# fac20 = (0.2506867-0.06267)/(0.2303-0.06267)
# 
# print(fac5, fac10, fac15, fac20)
#===============================================================================



#################################################
########integrate to get current data at temps
#################################################
#===============================================================================
# #################################################
# ########'definitive' run here
# #################################################
# 
# 
# sigmas=[1e9*elementary_charge, 4e9*elementary_charge, 8e9*elementary_charge,  1.2e10*elementary_charge, 3e10*elementary_charge, 6e10*elementary_charge, 1e11*elementary_charge, 3e11*elementary_charge, 6e11*elementary_charge, 1e12*elementary_charge, 5e12*elementary_charge, 1e13*elementary_charge] ##surface charges
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/definitive_runs_hopefully/surfacechargeruns.npy'
# SCDatas = np.load(fileLoc)
#  
# roughnesses=[.08, .12, .15, .18, .22, .25, .28, .31, .35, .4, .45, .5] ##roughnesses
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/definitive_runs_hopefully/roughnessruns.npy'
# roughnessDatas = np.load(fileLoc)
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/definitive_runs_hopefully/roughnessrunsNchangedsites.npy'
# NSitesChangedDatas = np.load(fileLoc)
#  
# n = [1e16, 5e16, 1e17, 5e17, 1e18, 3e18, 6e18, 1e19, 5e19, 1e20, 5e20, 1e21] ##doping concentrations
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/definitive_runs_hopefully/dopingruns.npy'
# dopingDatas = np.load(fileLoc)
# 
# bnesses = [0, .00003, .00006, .0001, .0004, .0008, .0014, .002, .003, .0035, .004, .005, .006, .01] ##bunching values
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/definitive_runs_hopefully/bunchingruns.npy'
# bunchingDatas = np.load(fileLoc)
#  
# 
# figsizex = 10
# figsizey = 5
# 
# 
# l = 70
# w = 24
# t = 12
# offsetEnergy = 1.95
# energies=np.array([-offsetEnergy + 0.0003 * i for i in range(1460)])
# energiesIneV = energies*energyToeV #for plotting in different units
# Vs = np.array([offsetEnergy - 0.1 + 0.0003 * i for i in range(800)]) ###in Volts
# 
# Ts = np.array([1e-12])#, 1e-4, 1e-2, 1, 10, 100, 400]) ##try some temperatures
# 
# print(Ts*k)
# 
# 
# 
# 
# 
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/definitive_runs_hopefully/'
# SCDatas = np.load(fileLoc+'surfacechargeruns.npy')
# current = []
#  
# for t in range(len(Ts)):
#     for n in range(len(SCDatas)):
#         current.clear()
#         print('sigma:'+str(sigmas[n]))
#         for V in Vs:
#             print('V = '+str(V))
#             current.append(integrate(SCDatas[n], energiesIneV, V)) ##V is mu in eV
#             print('I = '+str(current[len(current)-1]))
#          
#          
#         fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
#         ax1 = fig.add_subplot(111)
#         ax1.plot(Vs, np.array(current))
#         ax1.set_xlabel('Voltage [V]', fontsize = '15')
#         ax1.set_ylabel(r'Current [$e^2$/h]', fontsize = '15')
#         ax1.set_title('I-V for sigma = '+'{:.3e}'.format(sigmas[n]), fontsize = '17')
#         ax1.grid()
#         fig.tight_layout()
#         plt.legend()
#         plt.show()
#         plt.savefig(fileLoc+'SCI'+str(n)+'T'+str(t)+'.png')
#         plt.close()
#      
#         np.save(fileLoc+'surfacechargecurrents'+str(n)+'T'+str(t)+'.npy', current)
#===============================================================================








#actual final stuff here

fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/newstuff1nm/finalSC/SCsrand'
sigmas=np.array([0, 1e11, 5e11, 1e12, 5e12, 1e13, 5e13, 1e14])*elementary_charge
current = []


figsizex = 10
figsizey = 5
linestyles = ['-', '-.', '--']
   
   
   
   

#===============================================================================
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
#            
# for n in range(len(sigmas)):
#     sigma = sigmas[n]
#     datas = np.load(fileLoc+'{:.3e}'.format(sigma)+'run.npy', allow_pickle = True)
#     Vs = datas[1][np.int(np.shape(datas[1])[0]*.01):np.int(np.shape(datas[1])[0]*.8)]
#           
#     current.clear()
#      
#     for V in Vs: ##integrate to get current-votage in an inefficient way
#         #print('V = '+str(V))
#         current.append(currentIntegrate(datas[0], datas[1], V)) ##V is mu
#         #print('I = '+str(current[len(current)-1]))
#          
#     if(n > 1):
#         #urbach tail fitting
#         #urbeVRanges = [[-2.405, -2.403], [-2.404, -2.400], [-2.40, -2.387], [-2.4, -2.37], [-2.38, -2.3], [-2.38, -2.2]] ##read off the logplot - the flat bit.  ordered in increasing sigma for sigma >= 8.011e-08
#         urbeVRanges = [[-2.405, -2.403], [-2.404, -2.400], [-2.40, -2.387], [-2.4, -2.37], [-2.38, -2.3], [-2.38, -2.2]] ##read off the logplot - the flat bit on the top of the curve.  ordered in increasing sigma for sigma >= 8.011e-08
#         urbVs = np.linspace(Vs[0], Vs[np.alen(Vs)-1], 1000)
#              
#         indexStart = np.where(Vs > urbeVRanges[n-2][0])[0][0]
#         indexEnd = np.where(Vs > urbeVRanges[n-2][1])[0][0] - 1
#         fits = scipy.optimize.curve_fit(f = urbTail, xdata=Vs[indexStart:indexEnd], ydata=np.log10(np.array(current))[indexStart:indexEnd], p0 = [0, 0])
#         fitA = fits[0][0]
#         print('Fit Exp Factor: '+'{:.3e}'.format(1/(np.log(10)*fitA)))
#         fitB = fits[0][1]
#         print('Fit prefactor: '+'{:.3e}'.format(np.exp(-1*np.log(10)*fitA*fitB)))
#         ax1.plot(Vs, np.array(urbTail(Vs, fitA, fitB)), color = 'gray', linestyle = ':', linewidth = 2)#, label = r'Exponential fit: a='+'{:.3e}'.format(fitA)+', b='+'{:.3e}'.format(fitB)
#              
#     #===========================================================================
#     # fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
#     # ax1 = fig.add_subplot(111)
#     #===========================================================================
#           
#     ax1.plot(Vs, np.log10(np.array(current)), label = r'$\sigma = $'+'{:.3e}'.format(datas[2])+r' C/cm$^2$', linestyle = linestyles[n%len(linestyles)], linewidth = 2)
#           
# ax1.set_xlabel('Voltage [V]', fontsize = '15')
# ax1.set_ylabel(r' Log$_{10}$ of Current [log$_{10}$A]', fontsize = '15')
# ax1.set_title("Log of current by voltage for nanowire with surface charge", fontsize = '17')
# ax1.grid()
# plt.xlim(-2.4158, -2.1)
# plt.ylim(-36.75, -4.326)
# #plt.xlim(xmin=-2.42)
# fig.tight_layout()
# plt.legend()
# #plt.savefig(fileLoc+'logcurrimg.png')
# plt.show()
# plt.close()
#===============================================================================
  

     
#===============================================================================
# ###temperature stuff?
#        
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
#             
# n = 5
# sigma = sigmas[n]
# dataschg = np.load(fileLoc+'{:.3e}'.format(sigma)+'run.npy', allow_pickle = True)
#        
# inEs = dataschg[1][150:2800] ###not the entire set to have more energies around the fermi energies
#        
#        
#        
# n = 0
# plt.plot(dataschg[1][150:2800], dataschg[0][150:2800],label= r'$\beta$='+r'$\infty$ (kT = 0 meV, T = 0 K)',linewidth=3, linestyle = linestyles[n%len(linestyles)])
#        
# for beta in [2000,500,200, 50]:
#     n=n+1
#     print('beta = '+str(beta))
#     Conductance, Fermi_E,trans,energies= Use_temp(dataschg[0], dataschg[1], inEs, beta)
#     plt.plot(Fermi_E,Conductance,label= r'$\beta$='+'{:.0f}'.format(beta)+' eV$^{-1}$, (kT = +'+'{:.3}'.format(1000/(beta))+' meV, T = '+'{:.3}'.format(1/(k*beta))+' K)',linewidth=3, linestyle = linestyles[n%len(linestyles)])
#     #pyplot.legend()
#        
#        
#        
#        
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title("Conductance for nanowire with surface charge of $\sigma = $"+'{:.3e}'.format(dataschg[2])+r' C/cm$^2$', fontsize = '17')
# ax1.grid()
# #plt.xlim(xmin=-2.42)
# fig.tight_layout()
# plt.legend()
# #plt.savefig(fileLoc+'currimgtempstuff.png')
# plt.show()
# plt.close()
#===============================================================================
  




fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/newstuff1nm/finalSC/SR'
NroughnessesAll=[2, 4, 7, 14, 28, 47, 104, 232, 430] ## nums of roughness added to data
current = []
  
   
#===============================================================================
#      
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
#           
# for n in range(len(NroughnessesAll)):
#     Nroughness = NroughnessesAll[n]
#     datas = np.load(fileLoc+'{:.3e}'.format(Nroughness)+'run.npy', allow_pickle = True)
#     Vs = datas[1][np.int(np.shape(datas[1])[0]*.01):np.int(np.shape(datas[1])[0]*.8)]
#          
#     current.clear()
#     
#     for V in Vs: ##integrate to get current-votage in an inefficient way
#         #print('V = '+str(V))
#         current.append(currentIntegrate(datas[0], datas[1], V)) ##V is mu
#         #print('I = '+str(current[len(current)-1]))
#         
#     if(n > 3):
#         #urbach tail fitting
#         urbeVRanges = [[-2.404, -2.398], [-2.404, -2.397], [-2.4, -2.38], [-2.39, -2.355], [-2.38, -2.3]] ##read off the logplot - the flat bit at start of conductance step.  ordered in increasing roughs
#         urbVs = np.linspace(Vs[0], Vs[np.alen(Vs)-1], 1000)
#               
#         indexStart = np.where(Vs > urbeVRanges[n-4][0])[0][0]
#         indexEnd = np.where(Vs > urbeVRanges[n-4][1])[0][0] - 1
#         fits = scipy.optimize.curve_fit(f = urbTail, xdata=Vs[indexStart:indexEnd], ydata=np.log10(np.array(current))[indexStart:indexEnd], p0 = [0, 0])
#         fitA = fits[0][0]
#         print('Fit Exp Factor: '+'{:.3e}'.format(1/(np.log(10)*fitA)))
#         fitB = fits[0][1]
#         print('Fit prefactor: '+'{:.3e}'.format(np.exp(-1*np.log(10)*fitA*fitB)))
#         ax1.plot(Vs, np.array(urbTail(Vs, fitA, fitB)), color = 'gray', linestyle = ':', linewidth = 2)#, label = r'Exponential fit: a='+'{:.3e}'.format(fitA)+', b='+'{:.3e}'.format(fitB)
#             
#     #===========================================================================
#     # fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
#     # ax1 = fig.add_subplot(111)
#     #===========================================================================
#          
#     ax1.plot(Vs, np.log10(np.array(current)), label = r'N$_{r}$ = '+'{:.0f}'.format(datas[2]) + ' ('+'{:.2e}'.format(datas[2]/3772*(1e-2/1e-9)**2)+' cm$^{-2}$)', linestyle = linestyles[n%len(linestyles)], linewidth = 2)
#         
# ax1.set_xlabel('Voltage [V]', fontsize = '15')
# ax1.set_ylabel(r' Log$_{10}$ of Current [log$_{10}$A]', fontsize = '15')
# ax1.set_title("Log of current by voltage for nanowire with surface roughness", fontsize = '17')
# ax1.grid()
# plt.xlim(-2.4158, -2.15)
# plt.ylim(-30, -4.326)
# fig.tight_layout()
# plt.legend()
# #plt.savefig(fileLoc+'logcurrimg.png')
# plt.show()
# plt.close()
#    
#===============================================================================
  

#===============================================================================
# ###temperature stuff?
#    
# fig = plt.figure( figsize=(figsizex, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax1 = fig.add_subplot(111)
#         
# n = 5
# Nroughness = NroughnessesAll[n]
# datasr = np.load(fileLoc+'{:.3e}'.format(Nroughness)+'run.npy', allow_pickle = True)
#    
# inEs = datasr[1][150:2800] ###not the entire set to have more energies around the fermi energies
#    
#    
#    
# n = 0
# plt.plot(datasr[1][150:2800], datasr[0][150:2800],label= r'$\beta$='+r'$\infty$ (kT = 0 meV, T = 0 K)',linewidth=3, linestyle = linestyles[n%len(linestyles)])
#    
# for beta in [2000,500,200, 50]:
#     n=n+1
#     print('beta = '+str(beta))
#     Conductance, Fermi_E,trans,energies= Use_temp(datasr[0], datasr[1], inEs, beta)
#     plt.plot(Fermi_E,Conductance,label= r'$\beta$='+'{:.0f}'.format(beta)+' eV$^{-1}$, (kT = +'+'{:.3}'.format(1000/(beta))+' meV, T = '+'{:.3}'.format(1/(k*beta))+' K)',linewidth=3, linestyle = linestyles[n%len(linestyles)])
#     #pyplot.legend()
#  
#  
#  
#  
# ax1.set_xlabel('Energy [eV]', fontsize = '15')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '15')
# ax1.set_title('Conductance for nanowire with surface roughness of N$_{r}$ = '+'{:.0f}'.format(datasr[2]) + ' ('+'{:.2e}'.format(datasr[2]/3772*(1e-2/1e-9)**2)+r' cm$^{-2}$)', fontsize = '17')
# ax1.grid()
# #plt.xlim(xmin=-2.42)
# fig.tight_layout()
# plt.legend()
# #plt.savefig(fileLoc+'currimgtempstuff.png')
# plt.show()
# plt.close()
#===============================================================================





#===============================================================================
# ###temperature stuff combined plot
# mpl.rcParams['xtick.labelsize'] = 12
# mpl.rcParams['ytick.labelsize'] = 12
# 
#  
# fig = plt.figure( figsize=(figsizex*1.5, figsizey), dpi=80, facecolor='w', edgecolor='k')
# ax = fig.add_subplot(111, frameon=False)
# ax1 = fig.add_subplot(121)
# ax2 = fig.add_subplot(122)
# ax.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
#  
#  
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/newstuff1nm/finalSC/SCsrand'
# n = 5
# sigma = sigmas[n]
# dataschg = np.load(fileLoc+'{:.3e}'.format(sigma)+'run.npy', allow_pickle = True)
# inEs = dataschg[1][150:2800] ###not the entire set to have more energies around the fermi energies
# 
# n = 0
# ax1.plot(dataschg[1][150:2800], dataschg[0][150:2800],label= r'$\beta$='+r'$\infty$ (kT = 0 meV, T = 0 K)',linewidth=3, linestyle = linestyles[n%len(linestyles)])
#        
# for beta in [2000,500,200, 50]:
#     n=n+1
#     print('beta = '+str(beta))
#     Conductance, Fermi_E,trans,energies= Use_temp(dataschg[0], dataschg[1], inEs, beta)
#     ax1.plot(Fermi_E,Conductance,label= r'$\beta$='+'{:.0f}'.format(beta)+' eV$^{-1}$, (kT = +'+'{:.3}'.format(1000/(beta))+' meV, T = '+'{:.3}'.format(1/(k*beta))+' K)',linewidth=3, linestyle = linestyles[n%len(linestyles)])
#      
#      
# fileLoc = 'C:/Users/Administrator/\'Work\'/Cl/FYSM30/work/data/newstuff1nm/finalSC/SR'
# n = 5
# Nroughness = NroughnessesAll[n]
# datasr = np.load(fileLoc+'{:.3e}'.format(Nroughness)+'run.npy', allow_pickle = True)
# inEs = datasr[1][150:2800] ###not the entire set to have more energies around the fermi energies
#    
# n = 0
# ax2.plot(datasr[1][150:2800], datasr[0][150:2800],label= r'$\beta$='+r'$\infty$ (kT = 0 meV, T = 0 K)',linewidth=3, linestyle = linestyles[n%len(linestyles)])
#    
# for beta in [2000,500,200, 50]:
#     n=n+1
#     print('beta = '+str(beta))
#     Conductance, Fermi_E,trans,energies= Use_temp(datasr[0], datasr[1], inEs, beta)
#     ax2.plot(Fermi_E,Conductance,label= r'$\beta$='+'{:.0f}'.format(beta)+' eV$^{-1}$, (kT = +'+'{:.3}'.format(1000/(beta))+' meV, T = '+'{:.3}'.format(1/(k*beta))+' K)',linewidth=3, linestyle = linestyles[n%len(linestyles)])
# 
# 
#           
# ax.set_xlabel('Energy [eV]', fontsize = '17')
# ax1.set_title("Surface charge of $\sigma = $"+'{:.3e}'.format(dataschg[2])+r' C/cm$^2$', fontsize = '17')
# ax2.set_title('Surface roughness of N$_{r}$ = '+'{:.0f}'.format(datasr[2]) + ' ('+'{:.2e}'.format(datasr[2]/3772*(1e-2/1e-9)**2)+r' cm$^{-2}$)', fontsize = '17')
# ax1.set_ylabel(r'Conductance [$e^2$/h]', fontsize = '17')
# fig.suptitle("Conductance by energy for nanowire with imperfections at different temperatures", fontsize = '20')
# ax1.grid()
# ax2.grid()
# fig.tight_layout()
# ax1.legend(fontsize = '10')
# plt.show()
# plt.close()
#===============================================================================



print('Finish')